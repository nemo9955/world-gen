

import { CellGeography, GeoPosition, CellNeighbours, IsCell } from "../Planet/PComponents";
import { World, System, Component, TagComponent, Entity, Not } from "ecsy";
import * as d3geo from "d3-geo";

export function get_cell_id(entity_: Entity) {
    return entity_.id
}


function distance(a, b) {
    return Math.hypot(a[0] - b[0], a[1] - b[1]);
}


function sphereDistance(a, b) {
    return d3geo.geoDistance(a, b)
}


function get_cost_geo_distance(p1_, p2_) {
    var p1_pos_ = p1_.getComponent(GeoPosition) as GeoPosition
    var p2_pos_ = p2_.getComponent(GeoPosition) as GeoPosition


    // var e1_ = p1_.elevation
    // var e2_ = p2_.elevation

    var dist_ = sphereDistance(p1_pos_.asArray(), p2_pos_.asArray())
    const cost_dis_ = Math.ceil(dist_ * 10)
    // console.log("cost_dis_", cost_dis_);
    return [cost_dis_, cost_dis_]

    // var cost_e12_ = this.costs_of_2_elevations(e1_, e2_)
    // var cost_e21_ = this.costs_of_2_elevations(e2_, e1_)

    // var cost_1_ = cost_e12_
    // var cost_2_ = cost_e21_

    // cost_1_ *= cost_dis_
    // cost_2_ *= cost_dis_

    // return [cost_1_, cost_2_]
}



function add_double_edge(gr, e1, e2, c1, c2) {
    gr.sources.push(e1);
    gr.targets.push(e2);
    gr.costs.push(c1);
    gr.sources.push(e2);
    gr.targets.push(e1);
    gr.costs.push(c2);
    // gr.listed.push([e1, e2, c1])
    // gr.listed.push([e2, e1, c2])
}



export function neighbor_ents_to_graph(entities_: Array<Entity>) {

    var world_graph = { sources: [], targets: [], costs: [], listed: [] };

    var id_to_ind_ = {}
    entities_.forEach((cell_: Entity, cindex_) => {
        id_to_ind_[cell_.id] = cindex_
    });

    entities_.forEach((cell_: Entity, cindex_) => {
        var cpos_ = cell_.getComponent(GeoPosition)
        var cneighs_ = cell_.getComponent(CellNeighbours)

        cneighs_.neighbours.forEach((ncell_: Entity) => {
            var nindex_ = id_to_ind_[ncell_.id]
            var npos_ = ncell_.getComponent(GeoPosition)


            var [cost_1_, cost_2_] = get_cost_geo_distance(cell_, ncell_)


            add_double_edge(world_graph, cindex_, nindex_, cost_1_, cost_2_)

        });
    });

    // console.log("world_graph", world_graph);

    return world_graph
}



// export function voronoi_to_graph(voronoi_obj: any, voronoi_map: any) {

//     var world_graph = { sources: [], targets: [], costs: [], listed: [] };


//     console.log("voronoi_obj", voronoi_obj);
//     console.log("voronoi_map", voronoi_map);

//     console.log("voronoi_obj.links()", voronoi_obj.links());


//     voronoi_obj.links().features.forEach(lnk_ => {
//         var prop_ = lnk_.properties

//     });


//     return null
// }


