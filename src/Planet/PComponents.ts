
import { World, System, Component, TagComponent, Entity, Not } from "ecsy";

import * as d3geo from "d3-geo";
import * as turf from "@turf/turf";

import * as uconv from "../utils/Convert";

export class CellGeography extends Component {
    elevation: number;
    age: number;


    constructor() {
        super();
        this.reset()
    }
    reset() {
        this.elevation = 0
        this.age = -1
    }
}



export class BaseSpherePoint extends Component {
    y: number;
    x: number;
    size: number;
    constructor() { super(); this.reset() }
    reset() {
        this.y = 0
        this.x = 0
        this.size = 5
    }

    public distance = (pt_: GeoPosition) => {
        var from = turf.point([this.x, this.y]);
        var to = turf.point([pt_.x, pt_.y]);
        return turf.distance(from, to);
        // return d3geo.geoDistance([pt_.x, pt_.y], [this.x, this.y]) * 180 / Math.PI
    }

    public move = (dist_: number, dir_: number) => {
        var point = turf.point([this.x, this.y]);
        var destination = turf.destination(point, dist_, dir_);
        // console.log("destination", destination);
        [this.x, this.y] = destination.geometry.coordinates
    }

    public asArray = () => {
        return [this.x, this.y]
    }

    public set = (arr_) => {
        [this.x, this.y] = arr_
    }

    public geoPoint = () => {
        return turf.point([this.x, this.y], { size: this.size })

        // return {
        //     type: "Point",
        //     coordinates: [this.x, this.y],
        //     properties: {
        //         size: this.size
        //     }
        // }
    }

}

export class GeoPosition extends BaseSpherePoint { }

export class CellBorder extends Component {
    type = "Polygon"
    coordinates_ = new Array()
    area = 0
    perimeter = 0

    constructor() { super(); this.reset() }
    reset() {
        this.type = "Polygon"
        this.coordinates_.length = 0
        this.area = 0
        this.perimeter = 0
    }


    public get coordinates(): Array<any> {
        return this.coordinates_;
    }
    public set coordinates(v: Array<any>) {
        this.coordinates_ = v;
        var polygon = turf.polygon(v);
        this.area = Math.round(turf.area(polygon) / 1000000000);
        this.perimeter = Math.round(turf.length(polygon));
    }


}



export class CellNeighbours extends Component {
    neighbours = new Array<Entity>()
    distances = new Array<number>()
    constructor() {
        super();
        this.reset()
    }
    reset() {
        this.neighbours.length = 0
        this.distances.length = 0
    }
}


export class IsCell extends TagComponent { }

export class BadCell extends TagComponent { }



export class GeoDirection extends Component {
    public value = 0
    public scale = 1

    reset() {
        this.value = 0;
        this.scale = 1;
    }

    public geoArrow = (pos, scale = this.scale) => {

        var pto = turf.point([pos.x, pos.y])
        const distance = 500;
        const wing = 160
        const p1 = turf.destination(pto, distance * 3, 0 + this.value).geometry.coordinates;
        const p2 = turf.destination(pto, distance * 2, wing + this.value).geometry.coordinates;
        const p3 = turf.destination(pto, distance * 1, 180 + this.value).geometry.coordinates;
        const p4 = turf.destination(pto, distance * 2, -wing + this.value).geometry.coordinates;
        const pc = pto.geometry.coordinates;

        var line = turf.lineString([p1, p2, p3, p4]);
        var poly = turf.lineToPolygon(line);

        // console.log("turf.pointsWithinPolygon)", turf.pointsWithinPolygon(turf.points([[0, 90], [0, -90]]), poly));
        // if (turf.pointsWithinPolygon(turf.points([[0, 90], [0, -90]]), poly).features.length > 0)
        //     return pto

        var options = { pivot: pc, origin: pc, mutate: false };
        poly = turf.transformScale(poly, scale, options);



        // var pto = turf.point(uconv.wrapLatLon([pos.x, pos.y]))
        // const distance = 500;
        // const wing = 160
        // const p1 = uconv.wrapLatLon(turf.destination(pto, distance * 3, 0 + this.value).geometry.coordinates);
        // const p2 = uconv.wrapLatLon(turf.destination(pto, distance * 2, wing + this.value).geometry.coordinates);
        // const p3 = uconv.wrapLatLon(turf.destination(pto, distance * 1, 180 + this.value).geometry.coordinates);
        // const p4 = uconv.wrapLatLon(turf.destination(pto, distance * 2, -wing + this.value).geometry.coordinates);
        // const pc = uconv.wrapLatLon(pto.geometry.coordinates);

        // var line = turf.lineString([p1, p2, p3, p4]);
        // var poly = turf.lineToPolygon(line);

        // // console.log("turf.pointsWithinPolygon)", turf.pointsWithinPolygon(turf.points([[0, 90], [0, -90]]), poly));
        // // if (turf.pointsWithinPolygon(turf.points([[0, 90], [0, -90]]), poly).features.length > 0)
        // //     return pto

        // var options = { pivot: pc, origin: pc, mutate: false };
        // poly = turf.transformScale(poly, scale, options);


        // var pto = turf.point([pos.x, pos.y])
        // const distance = 500;
        // const wing = 140
        // const p1 = turf.destination(pto, distance * 3, 0).geometry.coordinates;
        // const p2 = turf.destination(pto, distance, wing).geometry.coordinates;
        // const p3 = pto.geometry.coordinates;
        // const p4 = turf.destination(pto, distance, -wing).geometry.coordinates;

        // const pc = uconv.wrapLatLon(pto.geometry.coordinates);

        // var line = turf.lineString([p1, p2, p3, p4]);
        // var poly = turf.lineToPolygon(line);
        // console.log("poly", poly);

        // var options = { pivot: pc, origin: pc, mutate: false };
        // poly = turf.transformRotate(poly, this.value, options);
        // poly = turf.transformScale(poly, scale, options);




        // // TODO cache the polygon shape and move it
        // const x = pos.x
        // const y = pos.y
        // // const w = (scale > 0.7 ? 4 : 5)
        // const w = 4
        // const h = 8
        // const h2 = 2
        // const p1 = [x, y + h + h2]
        // const p2 = [x + w, y - h + h2]
        // const p3 = [x, y - (h * 0.5) + h2]
        // const p4 = [x - w, y - h + h2]
        // const p5 = [...p1]
        // var arr_ = [p1, p2,  p4, p5]
        // // var arr_ = [p1, p2, p3, p4, p5]
        // // if (scale < 0.7)
        // //     arr_ = [p1, p2, p4, p5]
        // // arr_.reverse()
        // console.log("=======\\/======");
        // console.log("arr_", arr_.toString());
        // // arr_ = arr_.map(([x_, y_]) => {
        // //     console.log([x_, y_], " >>> ", uconv.wrapLatLon(x_, y_));
        // //     // return [x_, uconv.wrap90(y_)]
        // //     return uconv.wrapLatLon(x_, y_)
        // // })
        // // console.log("arr_", arr_.toString());
        // var line = turf.lineString(arr_);
        // var poly = turf.lineToPolygon(line);
        // // var poly = turf.polygon([arr_]);
        // // console.log("poly", poly);
        // // var options = { mutate: false };
        // var options = { pivot: [x, y], mutate: true };
        // // turf.cleanCoords(poly, options);
        // // turf.transformTranslate(poly, 0.1, 0, options);
        // // console.log("arr_", arr_.toString());
        // // turf.transformTranslate(poly, 2000, 45, options);
        // // turf.transformTranslate(poly, 2000, -45, options);

        //  turf.transformRotate(poly, this.value, options);
        //  turf.transformScale(poly, scale, options);

        // console.log("arr_", arr_.toString());
        // console.log("poly", poly);
        return poly
    }
}

