

import { World, System, Component, TagComponent, Entity, Not } from "ecsy";


import { CellGeography, GeoPosition, CellNeighbours, IsCell } from "./PComponents";


import * as uconvert from "../utils/Convert";


import { Planet } from "./Planet";


import * as noise_lib from 'noisejs';

import * as ptgen from "../utils/PointGenerate";
import * as d3g from "d3-geo-voronoi";
import * as d3geo from "d3-geo";
import * as d3 from "d3";


const NEEDS_GEO_QUERY = [IsCell, Not(CellGeography)];


export class UpdCellGeography extends System {
    public planet_: Planet

    // init(){}

    execute(delta, time) {
        var cell_geo_ = this.queries.cell_geo;

        cell_geo_.results.forEach(ent_ => {
            // console.log("ent_", ent_);
            // if(ent_.hasComponent(CellNeighbours)){
            //                 }
            var neighs_ = ent_.getComponent(CellNeighbours)
            // console.log("neighs_", neighs_);

            var esum = 0, ecnt = 0;

            neighs_.neighbours.forEach(nent_ => {
                if (nent_.hasComponent(CellGeography)) {
                    var ngeo_ = nent_.getComponent(CellGeography)
                    esum += ngeo_.elevation
                    ecnt++
                }
            });

            // console.log("esum, ecnt", esum, ecnt, neighs_.neighbours.length, Math.floor(esum / ecnt));

            ent_.addComponent(CellGeography)
            var geo_ = ent_.getMutableComponent(CellGeography) as CellGeography
            geo_.elevation = Math.floor(esum / ecnt)
            geo_.age = this.planet_.planet_age


        });

    }

    queries: any
    static queries = {
        cell_geo: {
            components: NEEDS_GEO_QUERY,
            // listen: {
            //     added: true,
            //     removed: true,
            //     changed: [GeoPosition]
            // }
        }
    }
}


export function register_systems(planet_: Planet) {
    planet_.regPlanetSystem(UpdCellGeography, -9500)
}

const NOISE_SIZE = 10
const NOISE_SEED = Math.random()
// const NOISE_SEED = 1234

export function seed_initial_cells(planet_: Planet) {
    var noise = new noise_lib.Noise(NOISE_SEED);
    // console.log("NOISE_SEED", NOISE_SEED)

    var all_cells_ = planet_.query_entities(NEEDS_GEO_QUERY);
    all_cells_.forEach(cell_ => {
        init_cell_geo(planet_, noise, cell_)
    });

}



function init_cell_geo(planet_: Planet, noise: noise_lib.Noise, cell_: Entity) {



    var pos_ = cell_.getComponent(GeoPosition) as GeoPosition

    var cco = uconvert.cartesian(pos_.asArray(), NOISE_SIZE / 10)
    cco = cco.map(element => { return 100 + element });

    // console.log("cco", cco);

    var pnoise = noise.perlin3(cco[0], cco[1], cco[2])
    pnoise = Math.floor(((pnoise + 1) / 2) * 100)


    cell_.addComponent(CellGeography)
    var geo_ = cell_.getMutableComponent(CellGeography) as CellGeography
    // geo_.elevation = Math.floor(Math.random() * 100)
    geo_.elevation = pnoise
    geo_.age = planet_.planet_age
}

