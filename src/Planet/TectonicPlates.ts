




import { CellGeography, GeoPosition, GeoDirection, CellNeighbours, IsCell } from "./PComponents";
import { World, System, Component, TagComponent, Entity, Not } from "ecsy";

import { Planet } from "./Planet";
import * as umisc from "../utils/Misc";
import * as uconv from "../utils/Convert";

import * as dju from "../../libs/dij_utils.js";

import * as ptgen from "../utils/PointGenerate";
import * as d3g from "d3-geo-voronoi";
import * as d3geo from "d3-geo";
import * as d3 from "d3";
import * as d3t from "d3-tricontour";
import { pack } from "d3";


class TecPlateRoot extends TagComponent { }


class TectPlateCell extends Component {
    public root: Entity
    reset() {
        this.root = null;
    }
}

class MovableTecPlate extends GeoDirection {
    private speed_ = 0;
    reset() {
        this.speed_ = 0;
    }

    public get speed(): number { return this.speed_; }
    public set speed(v: number) { this.speed_ = v; super.scale = Math.abs(this.speed_ * 0.5) + 0.5 }
}



class DrawTectPlateRoot extends System {
    public planet_: Planet
    public draw_surf_: any
    execute(delta, time) {


        // console.log(">>>>>","DrawTectPlateRoot");

        this.draw_surf_ = this.draw_surf_
            .data(this.queries.drawable.results, umisc.get_cell_id)
            .join(
                enter => enter.append("path")
                    .attr("d", this.get_point_fun)
                    .attr('stroke-width', "2px")
                    // .attr("fill", "green")
                    // .style("stroke", "transparent")
                    .style("stroke", "blue")
                    .style("fill", (ent_) => {
                        return d3.schemeDark2[ent_.id % d3.schemeDark2.length];
                    })
                , update => update
                    .attr("d", this.get_point_fun)
                    .attr('stroke-width', "2px")
                    // .attr("fill", "green")
                    // .style("stroke", "transparent")
                    .style("stroke", "black")
                    .style("fill", (ent_) => {
                        return d3.schemeDark2[ent_.id % d3.schemeDark2.length];
                    })
                , exit => exit.remove()
            )

    }
    get_point_fun = (entity) => {
        var pos = entity.getComponent(GeoPosition);
        return this.planet_.draw.geoPath(pos.geoPoint())
    }
    queries: any
    static queries = {
        drawable: {
            components: [TecPlateRoot, GeoPosition]
        }
    }
}




class DrawTectPlateContour extends System {
    public planet_: Planet
    public draw_surf_: any
    execute(delta, time) {

        // console.log(">>>>>","DrawTectPlateContour");

        // var tecgr = {}
        // var tecids = []
        var coords_id_list_ = []
        var all_plate_ids_ = []

        this.queries.drawable.results.forEach(ent_ => {

            var plroot = ent_.getComponent(TectPlateCell) as TectPlateCell
            var pos_ = ent_.getComponent(GeoPosition) as GeoPosition

            // if (!tecgr?.[plroot.root.id]) {
            //     tecgr[plroot.root.id] = []
            //     tecids.push(plroot.root.id)
            // }
            // tecgr[plroot.root.id].push(pos_.asArray())

            all_plate_ids_.push(plroot.root.id)

            var packet_ = []
            packet_ = packet_.concat(pos_.asArray())
            packet_ = packet_.concat([plroot.root.id])
            coords_id_list_.push(packet_)




        });
        all_plate_ids_ = [...new Set(all_plate_ids_)];

        // console.log("coords_id_list_", coords_id_list_);
        // console.log("all_plate_ids_", all_plate_ids_);

        var cont_points = []

        all_plate_ids_.forEach(baseid_ => {

            var pt_cont = d3g.geoContour()
                .value((obj_, ind_) => {
                    //    console.log("obj_, ind_", obj_, ind_);
                    return (obj_[2] === baseid_ ? 1 : 0)
                })
                // .thresholds(d3.range(-300, 300, 50))
                // .thresholds(1)
                .contour(coords_id_list_, 0.6)

            // console.log("pt_cont", pt_cont);

            cont_points.push(pt_cont)
        });

        // console.log("cont_points", cont_points);

        this.draw_surf_ = this.draw_surf_.data(cont_points)
            .join(
                enter => enter.append("path")
                    .style("stroke", "#111")
                    .attr('stroke-width', "3px")
                    .attr("fill", "rgba(0,0,0,0)")
                    // .attr("fill", color)
                    .attr("d", this.get_point_fun),
                update => update.attr("d", this.get_point_fun),
                exit => exit.remove()
            )


    }
    get_point_fun = (entity) => {
        return this.planet_.draw.geoPath(entity)
    }
    queries: any
    static queries = {
        drawable: {
            components: [IsCell, GeoPosition, TectPlateCell]
        }
    }
}



class DrawTectPlateCell extends System {
    public planet_: Planet
    public draw_surf_: any
    execute(delta, time) {

        // console.log(">>>>>","DrawTectPlateCell");

        this.draw_surf_ = this.draw_surf_
            .data(this.queries.drawable.results, umisc.get_cell_id)
            .join(
                enter => enter.append("path")
                    .attr("d", this.get_point_fun)
                    .attr("fill", "red")
                    .attr('stroke-width', "1px")
                    .style("stroke", (ent_) => {
                        var plroot = ent_.getComponent(TectPlateCell) as TectPlateCell
                        // console.log(" d3.schemeDark2[plroot.root.id]",  plroot.root.id, d3.schemeDark2[plroot.root.id]);
                        return d3.schemeDark2[plroot.root.id % d3.schemeDark2.length];
                    })
                , update => update
                    .attr("d", this.get_point_fun)
                    .attr("fill", "transparent")
                    .attr('stroke-width', "2px")
                    .style("stroke", (ent_) => {
                        var plroot = ent_.getComponent(TectPlateCell) as TectPlateCell
                        // console.log(" d3.schemeDark2[plroot.root.id]",  plroot.root.id, d3.schemeDark2[plroot.root.id]);
                        return d3.schemeDark2[plroot.root.id % d3.schemeDark2.length];
                    })
                , exit => exit.remove()
            )

    }
    get_point_fun = (entity) => {
        var pos = entity.getComponent(GeoPosition);
        return this.planet_.draw.geoPath(pos.geoPoint())
    }
    queries: any
    static queries = {
        drawable: {
            components: [IsCell, GeoPosition, TectPlateCell],
            // listen: {
            //     added: true,
            //     // removed: true,
            //     // changed: [GeoPosition]
            // }
        }
    }
}



class CellToTecPlate extends System {
    public planet_: Planet

    execute(delta, time) {

        this.queries.updateable.results.forEach(cell_ => {
            // console.log("cell_", cell_);
            var neighs_ = cell_.getComponent(CellNeighbours) as CellNeighbours

            var oldage_ = -100, oldcell_;

            neighs_.neighbours.forEach(ncell_ => {
                // console.log("ncell_", ncell_);
                if (ncell_.hasComponent(CellGeography)) {

                    var geo_ = ncell_.getComponent(CellGeography) as CellGeography;
                    // console.log("geo_", geo_);
                    if (geo_.age > oldage_ && ncell_.hasComponent(TectPlateCell)) {
                        oldage_ = geo_.age;
                        oldcell_ = ncell_;
                    }
                }
            });

            if (oldcell_ && oldcell_.hasComponent(TectPlateCell)) {
                var plate_ = oldcell_.getComponent(TectPlateCell) as TectPlateCell;
                cell_.addComponent(TectPlateCell, plate_)
            }

        });
    }

    queries: any
    static queries = {
        updateable: {
            components: [IsCell, GeoPosition, Not(TectPlateCell)]
        }
    }
}


class MoveTecPlate extends System {
    public planet_: Planet
    execute(delta, time) {

        console.log(">>>>>", "MoveTecPlate");

        this.queries.mvelems.results.forEach((elem_: Entity) => {
            var dir_ = elem_.getComponent(MovableTecPlate)
            var pos_ = elem_.getMutableComponent(GeoPosition)

            pos_.move(dir_.speed * 100, dir_.value)

        });
    }

    queries: any
    static queries = {
        mvelems: {
            components: [MovableTecPlate, GeoPosition]
        }
    }
}


class DrawTecPlateMvVec extends System {
    public planet_: Planet
    public draw_surf_: any
    execute(delta, time) {


        // console.log(">>>>>","DrawGeoVectors");

        this.draw_surf_ = this.draw_surf_
            .data(this.queries.drawable.results, umisc.get_cell_id)
            .join(
                enter => enter.append("path")
                    .attr("d", this.get_point_fun)
                    // .attr('stroke-width', "4px")
                    .attr("fill", "cyan")
                    .style("stroke", "black")
                , update => update
                    .attr("d", this.get_point_fun)
                    // .attr('stroke-width', "4px")
                    .attr("fill", "cyan")
                    .style("stroke", "black")
                , exit => exit.remove()
            )
    }

    get_point_fun = (entity) => {
        var pos = entity.getComponent(GeoPosition);
        var vec = entity.getComponent(MovableTecPlate);
        // return this.planet_.draw.geoPath(point_to_arrow(pos))
        return this.planet_.draw.geoPath(vec.geoArrow(pos))
    }
    queries: any
    static queries = {
        drawable: {
            // components: [MovableTecPlate, GeoPosition]
            components: [MovableTecPlate, GeoPosition, TecPlateRoot]
        }
    }
}



export function register_systems(planet_: Planet) {
    planet_.makeDrawSystem(DrawTectPlateContour, 1200);
    planet_.makeDrawSystem(DrawTectPlateRoot, 1200);
    planet_.regPlanetSystem(CellToTecPlate, -900);
    planet_.makeDrawSystem(DrawTectPlateCell, 1200);

    planet_.regPlanetSystem(MoveTecPlate, -2000);
    planet_.makeDrawSystem(DrawTecPlateMvVec, 1000);
}




export function make_tectonic_plates(planet_: Planet) {
    // console.log(">>>>>","make_tectonic_plates");
    planet_.query_entities([TectPlateCell]).forEach((ent_: Entity) => { (ent_ as any).remove(true); })
    planet_.query_entities([TecPlateRoot]).forEach((ent_: Entity) => { (ent_ as any).remove(true); })

    // var tect_pl = ptgen.make_sphere_points_rand_unif(6)
    var tect_pl = ptgen.poissonDiscSamplerSphere(9)


    var all_cells_ = planet_.query_entities([IsCell, CellNeighbours, GeoPosition])
    var graph_ = umisc.neighbor_ents_to_graph(all_cells_)
    // var graph_ = umisc.voronoi_to_graph(planet_.voronoi_obj, planet_.voronoi_map)


    var root_cells_ids_ = []
    var cell_to_root_ = {}
    tect_pl.forEach(plate_coords_ => {
        var base_cell_id_ = planet_.voronoi_obj.find(plate_coords_[0], plate_coords_[1])
        var root_cel_ = planet_.voronoi_map.itoe[base_cell_id_] as Entity

        var tplate_ = planet_.world.createEntity()
        tplate_.addComponent(GeoPosition, { x: plate_coords_[0], y: plate_coords_[1], size: 7 })
        tplate_.addComponent(TecPlateRoot)
        tplate_.addComponent(MovableTecPlate, { value: (Math.random() * 360), speed: 1 })

        root_cells_ids_.push(root_cel_.id)
        cell_to_root_[root_cel_.id] = tplate_
    });

    var root_cells_index_ = []
    all_cells_.forEach((ent_: Entity, index_) => {
        if (root_cells_ids_.includes(ent_.id)) {
            root_cells_index_.push(index_)
        }
    });


    // console.log("graph_", graph_);
    // console.log("root_cells_ids_", root_cells_ids_);
    // console.log("root_cells_index_", root_cells_index_);

    var tree = dju.shortest_tree({
        graph: graph_,
        origins: root_cells_index_,
        // cutoff: 40000,
    })
    var tree_fin = [...tree][0]


    // var lons = [0, 45, -45, 90, -90, -91, 91]
    // lons.forEach(lon => {
    //     console.log("lon", uconv.wrap90(lon), lon);
    //     console.log("");
    // });

    // console.log("");
    // console.log("");
    // console.log("");

    // var lats = [0, 50, 270, 360, -45, 380]
    // lats.forEach(lats => {
    //     console.log("lats", uconv.wrap360(lats), lats);
    //     console.log("");
    // });


    all_cells_.forEach((ent_: Entity, index_) => {
        // var tcel_ = planet_.voronoi_map.itoe[cell_] as Entity
        var cell_orig_index_ = tree_fin.origin[index_]
        var orig_cell_ = all_cells_[cell_orig_index_]
        var root_pt_ = cell_to_root_[orig_cell_.id]

        var dir_ = root_pt_.getComponent(MovableTecPlate);
        ent_.addComponent(MovableTecPlate, dir_)
        ent_.addComponent(TectPlateCell, { root: root_pt_ })
    });



}

