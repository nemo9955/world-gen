

import { World, System, Component, TagComponent, Entity, Not } from "ecsy";
import { CellGeography, GeoPosition, CellBorder, CellNeighbours, IsCell, BadCell } from "./PComponents";
import { get_cell_id } from "../utils/Misc";

import { Planet } from "./Planet";

import * as pcell from "./PlanetCells";
import * as cellgeo from "./CellGeography";

import * as ptgen from "../utils/PointGenerate";
import * as d3g from "d3-geo-voronoi";
import * as d3geo from "d3-geo";
import * as d3 from "d3";



class DrawCellPoition extends System {
    public planet_: Planet
    public draw_surf_: any

    // init(){}

    execute(delta, time) {


        // console.log(">>>>>","DrawCellBorder");

        this.draw_surf_ = this.draw_surf_
            .data(this.queries.drawable.results, get_cell_id)
            .join(
                enter => enter.append("path")
                    .attr("d", this.get_point_fun)
                    .attr("class", "point")
                    // .attr("fill", "green")
                    // .style("stroke", "transparent")
                    .attr("fill", "white")
                    .style("stroke", "red")
                    // .attr('opacity', "1")
                    .attr('stroke-width', "1px")
                , update => update
                    .attr("d", this.get_point_fun)
                    .attr("class", "point")
                    // .attr("fill", "green")
                    // .style("stroke", "transparent")
                    .attr("fill", "white")
                    .style("stroke", "red")
                    // .attr('opacity', "1")
                    .attr('stroke-width', "1px")
                , exit => exit.remove()
            )

    }


    get_point_fun = (entity) => {
        var pos = entity.getComponent(GeoPosition);
        return this.planet_.draw.geoPath(pos.geoPoint())
    }

    queries: any
    static queries = {
        drawable: {
            components: [IsCell, GeoPosition]
        }
    }
}







class DrawCellBorder extends System {
    public planet_: Planet
    public draw_surf_: any

    // init(){}

    execute(delta, time) {


        // console.log(">>>>>","DrawCellBorder");

        this.draw_surf_ = this.draw_surf_.data(this.queries.drawable.results, get_cell_id)
            .join(
                enter => enter.append("path")
                    .call(this.update_polygon)
                , update => update
                    .call(this.update_polygon)
                , exit => exit.remove()
            )


    }

    get_border_fun = (entity) => {
        var border_ = entity.getComponent(CellBorder);

        return this.planet_.draw.geoPath(border_)
    }


    update_polygon = (poly_: any) => {
        poly_
            .style("stroke", "#000")
            .attr("d", this.get_border_fun)
            // .attr("fill", "#f4f4f4")
            .attr('opacity', "0.6") // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            .attr("fill", function (ent_: Entity) {

                if (ent_.hasComponent(BadCell))
                    return "orange"

                if (!ent_.hasComponent(CellGeography))
                    return "yellow"

                var geo_ = ent_.getComponent(CellGeography);
                return d3.scaleLinear<string>()
                    .domain([0, 50, 100])
                    .range(["rgba(23,23,193, 1)", "lightgray", "green"])
                    (geo_.elevation)

            })
    }


    queries: any
    static queries = {
        drawable: {
            components: [IsCell, CellBorder]
        }
    }
}


export function register_systems(planet_: Planet) {
    planet_.makeDrawSystem(DrawCellBorder, 999);
    // planet_.makeDrawSystem(DrawCellPoition, 1000);
}

