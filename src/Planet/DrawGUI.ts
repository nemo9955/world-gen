

import { Planet } from "../Planet/Planet";

import * as dat from "dat.gui";

export class DrawGUI {

    gui: dat.GUI;

    constructor(public planet_: Planet) {
        this.gui = new dat.GUI();
        // this.gui.width = 500

        this.gui.add(this.planet_, 'aprox_cells_count', 1);

        this.gui.add(this.planet_, 'toggle_graticule');
        this.gui.add(this.planet_, 'run_once');

        this.gui.add(this.planet_, 'add_random_cell');
        this.gui.add(this.planet_, 'remove_random_cell');

    }




}