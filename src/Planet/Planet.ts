
import { CellGeography, GeoPosition, CellNeighbours, IsCell } from "./PComponents";

import * as pgrat from "./DrawGraticule";
import * as pcells from "./PlanetCells";
import * as drcells from "./DrawCells";
import * as pgeo from "./CellGeography";
import * as tecplates from "./TectonicPlates";
import * as gvec from "./GeoVectors";

import { DrawSVG } from "./DrawSVG";
import { DrawGUI } from "./DrawGUI";
import * as d3g from "d3-geo-voronoi";

import { World, System, Entity, Component } from "ecsy";
import * as d3 from "d3";


// export class PlanetComp extends Component {
//     value: Planet
//     constructor() { super(); this.reset(); }
//     public reset() { this.value = null; }
// }


export class Planet {

    public world: World;
    public draw: DrawSVG;

    lastTime: number;
    theGui: DrawGUI

    private planet_age_ = 0;
    private aprox_cells_count_: number;
    public cells_count: number;

    // public average_distance: number;
    // public average_area: number;
    public average_cell_perimeter: number;

    public voronoi_obj: d3g.geoVoronoi;
    public voronoi_map: any;

    constructor() {
        this.aprox_cells_count_ = 20

        this.theGui = new DrawGUI(this);
        this.draw = new DrawSVG();
        this.world = new World();

        drcells.register_systems(this)

        pcells.register_systems(this)
        pgeo.register_systems(this)
        gvec.register_systems(this)
        tecplates.register_systems(this)

        pgrat.register_systems(this)
        this.toggle_graticule()

        gvec.add_some_vectors(this)

        this.draw.add_resize_listener(this.run_once)
    }


    public init_planet = () => {
        this.make_base_cells();
        this.run_once()
    }

    public make_base_cells = () => {
        this.cells_count = pcells.populate_planet_cells(this, this.aprox_cells_count_);
        pgeo.seed_initial_cells(this)

        // pcells.computeCellsVoronoi(this, this.query_entities([IsCell]))
        this.run_once()

        tecplates.make_tectonic_plates(this)
        // this.run_once()
        this.run_once()
    }


    public regPlanetSystem = (type_: any, priority_ = 0) => {
        console.log("regPlanetSystem", type_.name, priority_);
        (this.world as any).registerSystem(type_, { priority: priority_ });
        var planet_sys_ = this.world.getSystem(type_) as any
        planet_sys_.planet_ = this
        return planet_sys_
    }

    public makeDrawSystem = (type_: any, priority_) => {
        var dr_sys_ = this.regPlanetSystem(type_, priority_)
        dr_sys_.draw_surf_ = this.draw.main_svg
            .append("g")
            .datum(priority_)
            .attr("type_", type_.name)
            .attr("priority_", priority_)
            .selectAll("*");
        this.draw.main_svg.selectAll("g").sort();
    }

    public toggle_graticule = () => {
        var grat_ = this.world.getSystem(pgrat.DrawGraticuleSystem) // as pgrat.DrawGraticuleSystem;
        if (grat_.enabled)
            grat_.stop();
        else {
            grat_.play();
            this.run_once()
        }
    }

    public add_random_cell = () => {
        pcells.add_world_cell(this, [Math.random() * 360, (Math.random() * 180) - 90])
        this.run_once()
    }

    public remove_random_cell = () => {
        var all_cells_ = this.query_entities([IsCell]);
        const rand_index = Math.floor(all_cells_.length * Math.random());
        var rand_ent_ = all_cells_[rand_index];
        rand_ent_.remove();
        this.run_once()
    }

    public query_entities = (gen_obj_) => {
        var all_ents_ = [];
        class DUMMY_SYSTEM extends System {
            execute(delta, time) {
                all_ents_ = [...this.queries.generic_.results]
            }

            queries: any
            static queries = {
                generic_: { components: gen_obj_ }
            }
        }
        this.world.registerSystem(DUMMY_SYSTEM);
        var dums: any = this.world.getSystem(DUMMY_SYSTEM);
        (this.world as any).systemManager.executeSystem(dums, 0, 0);
        (this.world as any).systemManager.removeSystem(dums);
        // (this.world as any).systemManager.removeSystem(DUMMY_SYSTEM);

        // console.log("all_ents_", all_ents_);
        // (this.world as any).stats();
        return all_ents_;

    }


    public get planet_age(): number { return this.planet_age_ }


    public get aprox_cells_count(): number {
        return this.aprox_cells_count_
    }
    public set aprox_cells_count(v: number) {
        this.aprox_cells_count_ = v;
        this.make_base_cells()
    }



    public start = () => {
        this.lastTime = performance.now();
        this.run();
    }

    private run = () => {
        this.run_once()
        requestAnimationFrame(this.run);
    }

    public run_once = () => {
        let time = performance.now();
        let delta = time - this.lastTime;

        this.world.execute(delta, time);

        this.planet_age_++;

        this.lastTime = time;
    }


}