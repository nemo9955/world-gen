


import * as d3 from "d3";


export class DrawSVG {


    public main_svg = d3.select('body').append('svg')

    view_width: number;
    view_height: number;

    projection: any;
    initial_scale: number;
    initial_rotate: { x: number; y: number; };
    geoPath: d3.GeoPath<any, d3.GeoPermissibleObjects>;



    constructor() {
        this.resize()

        this.initial_scale = 300;
        this.initial_rotate = {
            x: 0,
            y: -20
        };

        // this.projection = d3.geoTransverseMercator(); this.initial_scale = 110
        // this.projection = d3.geoAzimuthalEqualArea(); this.initial_scale = 150
        this.projection = d3.geoNaturalEarth1(); this.initial_scale = 150
        // this.projection = d3.geoOrthographic(); this.initial_scale = 300
        // this.projection = d3.geoMercator(); this.initial_scale = 130

        this.projection.scale(this.initial_scale)
            .translate([this.view_width / 2, this.view_height / 2])
            .rotate([this.initial_rotate.x, this.initial_rotate.y])
            .center([0, 0])

        this.geoPath = d3.geoPath()
            .projection(this.projection);


        this.geoPath.pointRadius(function (d: any) {
            // console.log(d)
            if (d?.properties?.size) return d.properties.size
            return 4;
        })


        d3.select(window).on("resize", this.resize)

    }


    public resize = () => {
        this.view_width = window.innerWidth - 50;
        this.view_height = window.innerHeight - 50;

        this?.main_svg
            .attr("width", this.view_width)
            .attr("height", this.view_height)

        this?.projection
            ?.scale(this.initial_scale)
            ?.translate([this.view_width / 2, this.view_height / 2])
            ?.center([0, 0])
        // .rotate([initial_rotate.x, initial_rotate.y])

        this.resize_listeners.forEach(element => {
            element()
        });

    }

    resize_listeners = []
    public add_resize_listener(callback_fun_: any) {
        this.resize_listeners.push(callback_fun_)
    }



}