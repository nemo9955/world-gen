

import { CellGeography, GeoPosition, CellBorder, CellNeighbours, IsCell, BadCell } from "./PComponents";
import { World, System, Component, TagComponent, Entity, Not } from "ecsy";

import { Planet } from "./Planet";
import * as turf from "@turf/turf";


import * as ptgen from "../utils/PointGenerate";
import * as d3g from "d3-geo-voronoi";
import * as d3geo from "d3-geo";
import * as d3 from "d3";
import { treemapSquarify } from "d3";



export class CellsDistantiate extends System {
    public planet_: Planet

    // init(){}

    execute(delta, time) {

        var clist_ = this.queries.cell_listen;

        if (this.planet_.voronoi_obj) {
            // console.log(">>>>>", "CellsDistantiate");
            // console.log("this.planet_.voronoi_obj", this.planet_.voronoi_obj);

            this.planet_, clist_.results.forEach((cell_: Entity) => {
                // var cneighs_ = cell_.getComponent(CellNeighbours)
                var cbord_ = cell_.getComponent(CellBorder)
                var pos_ = cell_.getComponent(GeoPosition)

                // if (cbord_.perimeter < this.planet_.average_cell_perimeter * 0.7) {
                //     console.log("this.planet_.average_cell_perimeter", this.planet_.average_cell_perimeter);
                //     console.log("cbord_.area >average_cell_perimeter", cbord_.perimeter, this.planet_.average_cell_perimeter * 0.7);
                //     // cell_.addComponent(BadCell);
                //     cell_.remove();
                // }

                if (cbord_.perimeter > this.planet_.average_cell_perimeter * 1.2) {
                    console.log("this.planet_.average_cell_perimeter", this.planet_.average_cell_perimeter);
                    console.log("cbord_.area >average_cell_perimeter", cbord_.perimeter, this.planet_.average_cell_perimeter * 1.2);
                    // cell_.addComponent(BadCell);
                    // cell_.remove();

                    // const numsplit = 3;
                    // for (let index = 0; index < numsplit; index++) {
                    //     // console.log("pos_.", pos_.geoPoint(), this.planet_.average_cell_perimeter / 1000, 360 / index);
                    //     var destination = turf.destination(pos_.geoPoint(), this.planet_.average_cell_perimeter / 5, 360 / (index + 1));
                    //     console.log("destination.geometry.coordinates", destination.geometry.coordinates);
                    //     if (index == 0) {
                    //         var cpos_ = cell_.getMutableComponent(GeoPosition)
                    //         cpos_.set(destination.geometry.coordinates)
                    //     } else {
                    //         add_world_cell(this.planet_, destination.geometry.coordinates as any)
                    //     }
                    // }
                }



                // cneighs_.distances.forEach((ndis_) => {
                //     if (ndis_ < this.planet_.average_distance * 0.5) {
                //         close_cells_.push(cell_)
                //     }
                // });

            });

            // if (close_cells_.length > 0) {
            //     console.log("close_cells_", close_cells_);
            //     close_cells_.forEach(element => {
            //         element.addComponent(BadCell);
            //         // element.remove();

            //     });
            // }

        }


    }

    queries: any
    static queries = {
        cell_listen: {
            components: [IsCell, GeoPosition, CellBorder],
            // listen: {
            //     added: true,
            //     removed: true,
            //     changed: [GeoPosition]
            // }
        }
    }

}


export class CellsVoronoi extends System {
    public planet_: Planet

    // init(){}

    execute(delta, time) {

        var clist_ = this.queries.cell_listen;

        // // All the entities with `Box` component
        // clist_.results.forEach(entity => { });

        // // All the entities added to the query since the last call
        // clist_.added.forEach(entity => { console.log("added entity", entity); });

        // // All the entities removed from the query since the last call
        // clist_.removed.forEach(entity => { console.log("removed entity", entity); });


        var needs_update = false
        if (clist_.added.length > 0) needs_update = true;
        if (clist_.removed.length > 0) needs_update = true;
        if (clist_.changed.length > 0) needs_update = true;

        // console.log("clist_.results.length ", clist_.results.length);
        // console.log("clist_.added.length ", clist_.added.length);
        // console.log("clist_.removed.length ", clist_.removed.length);
        // console.log("clist_.changed.length ", clist_.changed.length);


        if (needs_update) {
            // console.log(">>>>>", "CellsVoronoi");
            computeCellsVoronoi(this.planet_, clist_.results);
        }

    }

    queries: any
    static queries = {
        cell_listen: {
            components: [IsCell, GeoPosition],
            listen: {
                added: true,
                removed: true,
                changed: [GeoPosition]
            }
        }
    }

}



export function computeCellsVoronoi(planet_: Planet, ents_list_: Array<Entity>) {
    // console.log(">>>>>", "computeCellsVoronoi");

    var coords_ = ents_list_.map((ent_: Entity) => {
        var pos = ent_.getComponent(GeoPosition);
        return pos.asArray();
    });
    // console.log("coords_", coords_);
    planet_.voronoi_obj = d3g.geoVoronoi()(coords_);
    planet_.voronoi_map = {};
    planet_.voronoi_map.itoe = {};
    planet_.voronoi_map.etoi = {};
    // var nmap = {}

    var bord_dists = []

    planet_.voronoi_obj.polygons().features.forEach((cel_, ind_) => {
        var cell_: Entity = ents_list_[ind_];

        var cpos_ = cell_.getComponent(GeoPosition)

        planet_.voronoi_map.itoe[ind_] = cell_;
        planet_.voronoi_map.etoi[cell_.id] = ind_;
        if (cell_.hasComponent(CellBorder)) {
            var border: CellBorder = cell_.getMutableComponent(CellBorder);
            border.type = cel_.geometry.type;
            border.coordinates = cel_.geometry.coordinates;
        }
        else {
            cell_.addComponent(CellBorder, cel_.geometry);
        }
        var bord_: CellBorder = cell_.getComponent(CellBorder);
        bord_dists.push(bord_.perimeter)
        // bord_dists.push(bord_.area)

        var cneights_ = [];
        var cdists_ = [];
        cel_.properties.neighbours.forEach(nid_ => {
            // cell.neighbours.push(.cells[nid_])
            var ncel_ = ents_list_[nid_];
            var npos_ = ncel_.getComponent(GeoPosition)
            var ndist_ = cpos_.distance(npos_)
            cneights_.push(ncel_);
            cdists_.push(ndist_);
            // diss += ndist_ / 1000; disc++;
        });

        // console.log("cdists_", cdists_);

        if (cell_.hasComponent(CellNeighbours)) {
            var neighs: CellNeighbours = cell_.getMutableComponent(CellNeighbours);
            neighs.neighbours = [...cneights_];
            neighs.distances = [...cdists_];
        }
        else {
            cell_.addComponent(CellNeighbours, { neighbours: cneights_, distances: cdists_ });
        }
    });


    // planet_.average_distance = (diss / disc) * 1000
    // planet_.average_area = Math.round(arrs / arrc)

    console.log("d3.mean()", d3.mean(bord_dists));
    console.log("d3.median()", d3.median(bord_dists));
    console.log("d3.min()", d3.min(bord_dists));
    console.log("d3.max()", d3.max(bord_dists));
    console.log("d3.deviation()", d3.deviation(bord_dists));

    planet_.average_cell_perimeter = d3.median(bord_dists)

    var vorobj_ = planet_.voronoi_obj;
    // var delaunay = planet_.voronoi_obj;

    // console.log("planet_.voronoi_obj", planet_.voronoi_obj.delaunay);
    // console.log("planet_.voronoi_obj", planet_.voronoi_obj.points);

    vorobj_.findAll = function (x, y, radius) {
        const points = vorobj_.points,
            results = [],
            seen = [],
            queue = [vorobj_.find(x, y)];

        // console.log("radius", radius);

        while (queue.length) {
            // console.log("queue", queue);
            const q = queue.pop();
            if (seen[q]) continue;
            seen[q] = true;
            // var dist_ = Math.hypot(x - points[2 * q], y - points[2 * q + 1])
            var dist_ = d3geo.geoDistance([points[q][0], points[q][1]], [x, y]) * 180 / Math.PI
            // var dist_ = d3geo.geoDistance([points[2 * q], points[2 * q + 1]], [x, y])
            // console.log("dist_", dist_, [points[q][0], points[q][1]], [x, y]);
            if (dist_ < radius) {
                // if (d3geo.geoDistance([x, y], v.points[v._found]) < radius) {
                results.push(q);
                for (const p of vorobj_.delaunay.neighbors[q]) queue.push(p);
            }
        }
        // console.log("results", results);
        return results;
    }




}



export function add_world_cell(planet_: Planet, pt_: [number, number]) {
    var ent = planet_.world.createEntity();

    ent.addComponent(GeoPosition, { x: pt_[0], y: pt_[1], size: 3 })
    ent.addComponent(IsCell)

    return ent
}


function generate_cell_points(cells_count: number) {
    // console.log("Will gen aprox total ", cells_count)
    var sites = []

    // sites = sites.concat([[0, -90], [0, 90]])
    // sites = sites.concat([[0, 0]])
    // sites = sites.concat(ptgen.make_sphere_points_fib_phi(cells_count))
    sites = sites.concat(ptgen.poissonDiscSamplerSphere(cells_count))

    sites = ptgen.rm_dup_points(sites)

    // console.log("Total generated ", sites.length)
    return sites
}



export function register_systems(planet_: Planet) {
    // (planet_.world as any).registerSystem(CellsVoronoi, { priority: 50 });
    planet_.regPlanetSystem(CellsDistantiate, -1100);
    planet_.regPlanetSystem(CellsVoronoi, -10000);
}


export function populate_planet_cells(planet_: Planet, cells_count_: number) {

    var base_points_ = generate_cell_points(cells_count_)



    planet_.query_entities([IsCell]).forEach((ent_: Entity) => { (ent_ as any).remove(true); })

    base_points_.forEach((cel_) => {
        var cell_ = add_world_cell(planet_, cel_)
    });




    return base_points_.length
}
