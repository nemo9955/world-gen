
import { World, System, Component } from "ecsy";

import * as d3 from "d3";
import { DrawSVG } from "./DrawSVG";
import { Planet } from "./Planet";

const GRATICULE_PRIORITY = 1000

class GraticuleData extends Component {
    graticule: d3.GeoGraticuleGenerator;
    constructor() {
        super();
        this.graticule = d3.geoGraticule();
        this.reset()
    }
    reset() {
    }
}


export class DrawGraticuleSystem extends System {
    public planet_: Planet
    public draw_surf_: any

    // init(){}

    execute(delta, time) {
        this.queries.graticule.results.forEach(entity => {
            var grdata = entity.getMutableComponent(GraticuleData) as GraticuleData;

            // console.log("grdata", grdata);
            // console.log("this.draw_surf_", this.draw_surf_);

            this.draw_surf_ = this.draw_surf_.datum(grdata.graticule)
                .attr('d', this.planet_.draw.geoPath)
                .attr('fill', "none")
                .style("stroke", "red")
                .attr('stroke-width', "1px")
                .attr('stroke-opacity', ".2")
            // .join(
            //     enter => enter.append("path")
            //     , update => update
            //     , exit => exit.remove()
            // )
        });
    }

    public play = () => {
        super.play()
        this.draw_surf_.style("visibility", "visible")
    }

    public stop = () => {
        super.stop()
        this.draw_surf_.style("visibility", "hidden")
    }

    queries: any
    static queries = {
        graticule: {
            components: [GraticuleData]
        }
    }
}


export function register_systems(planet_: Planet) {
    var grsysstem = planet_.makeDrawSystem(DrawGraticuleSystem, 1100);
    // var grsysstem = planet_.regPlanetSystem(DrawGraticuleSystem, 1100)
    // grsysstem.draw_surf_ = planet_.draw.main_svg.append("g").append("path")

    var ent = planet_.world.createEntity()
    ent.addComponent(GraticuleData)
    return grsysstem
}
