




import { CellGeography, GeoPosition, GeoDirection, CellNeighbours, IsCell } from "./PComponents";
import { World, System, Component, TagComponent, Entity, Not } from "ecsy";

import { Planet } from "./Planet";
import * as umisc from "../utils/Misc";

import * as dju from "../../libs/dij_utils.js";

import * as ptgen from "../utils/PointGenerate";
import * as d3g from "d3-geo-voronoi";
import * as d3geo from "d3-geo";
import * as d3 from "d3";
import * as d3t from "d3-tricontour";
import { pack } from "d3";
import * as turf from "@turf/turf";




class DrawGeoVectors extends System {
    public planet_: Planet
    public draw_surf_: any
    execute(delta, time) {


        // console.log(">>>>>","DrawGeoVectors");

        this.draw_surf_ = this.draw_surf_
            .data(this.queries.drawable.results, umisc.get_cell_id)
            .join(
                enter => enter.append("path")
                    .attr("d", this.get_point_fun)
                    // .attr('stroke-width', "4px")
                    .attr("fill", "cyan")
                    .style("stroke", "black")
                , update => update
                    .attr("d", this.get_point_fun)
                    // .attr('stroke-width', "4px")
                    .attr("fill", "cyan")
                    .style("stroke", "black")
                , exit => exit.remove()
            )
    }

    get_point_fun = (entity) => {
        var pos = entity.getComponent(GeoPosition);
        var vec = entity.getComponent(GeoDirection);
        // return this.planet_.draw.geoPath(point_to_arrow(pos))
        return this.planet_.draw.geoPath(vec.geoArrow(pos))
    }
    queries: any
    static queries = {
        drawable: {
            components: [GeoDirection, GeoPosition]
        }
    }
}




export function register_systems(planet_: Planet) {
    planet_.makeDrawSystem(DrawGeoVectors, 2000);
}



export function add_some_vectors(planet_: Planet) {

    // var e1_ = planet_.world.createEntity() as Entity
    // e1_.addComponent(GeoPosition, { x: 0, y: -90 })
    // e1_.addComponent(GeoDirection, { value: 180, scale: 1 })

    // for (let j = 0; j < 5; j++) {
    //     for (let i = 0; i < 90; i++) {
    //         var e1_ = planet_.world.createEntity() as Entity
    //         // e1_.addComponent(GeoPosition, { x: 40, y: 50, size: 20 })
    //         e1_.addComponent(GeoPosition, { x: i * 2 - 80, y: (j * 30)+70 })
    //         e1_.addComponent(GeoDirection, { value: 90, scale: 0.5 })
    //     }
    // }


    // for (let j = -80; j <= 80; j += 10) {
    //     for (let i = 0; i <= 360; i += 10) {
    //         var e1_ = planet_.world.createEntity() as Entity
    //         e1_.addComponent(GeoPosition, { x: i, y: j })
    //         e1_.addComponent(GeoDirection, { value: j + i, scale: 0.5 })
    //     }
    // }

    // for (let j = -90; j <= 90; j += 5) {
    //     for (let i = 0; i <= 360; i += 10) {
    //         var e1_ = planet_.world.createEntity() as Entity
    //         e1_.addComponent(GeoPosition, { x: i, y: j })
    //         e1_.addComponent(GeoDirection, { value: j*31+i*9, scale: 0.5 })
    //     }
    // }
}
