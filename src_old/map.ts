
import * as d3 from "d3";


export abstract class MapBase {

    view_width: number
    view_height: number

    points_count: number

    noise_spread: number
    noise_seed: number
    noise_seed2: number

    relief_gradient: any

    // abstract calculate_altitude_colors: (min: number, max: number) => void



    show_elev_cost_heatmap: any = false
    heat_map_svg: any = null
    ignore_elevation_cost: any = false
    tooltip_div = d3.select("body").append("div")


    constructor() {
        // this.tooltip_div = d3.select("body").append("div")

    }



    public rm_dup_points = (points_) => {


        points_.sort((a, b) => a[1] - b[1])
        points_.sort((a, b) => a[0] - b[0])

        // console.log("points_", points_.length ,points_.toLocaleString())
        // console.log("points_", points_.length, points_)

        for (let index = 0; index < points_.length; index++) {
            const pt_ = points_[index]

            // points_.forEach(function (pt_, i, points_) {
            if (index + 1 >= points_.length) continue;
            var pt2_ = points_[index + 1]

            if (pt_[0] === pt2_[0] && pt_[1] === pt2_[1]) {
                console.log("Removing ", pt2_, " == ", pt_, " >>>", points_[index + 1])
                var len1 = points_.length
                var rmmm = points_.splice(index + 1, 1)
                var len2 = points_.length
                index--
                // console.log("dup", len1, len2, pt_, pt2_, "rmmm",rmmm.toLocaleString())
            }
        }
        // console.log("sites", sites.length ,sites.toLocaleString())

    }



    public costs_of_2_elevations = (elev1, elev2) => {

        var medelev = ((elev1 + elev2) / 2)
        var diff = Math.abs(elev1 - elev2)
        var dire = Math.sign(elev1 - elev2)


        var diff_fact = ((diff * 2) - (diff * dire * 0.2))
        diff_fact += Math.pow(diff_fact, 2)


        var elev_fact = ((Math.abs(medelev) - (medelev * 0.1)) * 1.7)
        elev_fact += Math.pow(elev_fact, 2)

        var cost = diff_fact + elev_fact
        cost = Math.sqrt(cost)

        if (this.ignore_elevation_cost)
            return 1
        return cost

    }

    public view_data = (lst_data = null, calc_mode = null) => {

        // var elevs = diagram.polygons().features.map(d => {
        //     return d.properties.site.properties.elevation
        // }).sort()

        var minmax_data: [number, number] = [null, null]
        var data = []

        if (this.ignore_elevation_cost || lst_data == null) {

            var old_val = this.ignore_elevation_cost
            this.ignore_elevation_cost = false

            var mm_val = 10

            var elevs = d3.range(-mm_val, mm_val + 1, mm_val / 20)
            elevs.forEach(e1 => {
                e1 = Math.round(e1)
                elevs.forEach(e2 => {
                    e2 = Math.round(e2)
                    var ppp = [
                        calc_mode(e1, e2),
                        calc_mode(e2, e1)
                    ]
                    ppp = [Math.round(ppp[0]), Math.round(ppp[1])]
                    data.push([e1, e2, ppp[0]])
                    data.push([e2, e1, ppp[1]])
                    // console.log("minmax_data[0], ppp[0], ppp[1]", minmax_data[0], ppp[0], ppp[1])
                    minmax_data[1] = Math.min(minmax_data[1], ppp[0], ppp[1])
                    minmax_data[0] = Math.max(minmax_data[0], ppp[0], ppp[1])
                });
            });
            this.ignore_elevation_cost = old_val

        }
        else {
            // console.log("lst_data", lst_data)
            data = lst_data.map(ppp => {
                ppp = [Math.round(ppp[0]), Math.round(ppp[1]), Math.round(ppp[2])]
                // console.log("ppp", ppp)
                // minmax_data[0] = Math.min(minmax_data[0], ppp[2])
                // minmax_data[1] = Math.max(minmax_data[1], ppp[2])
                minmax_data[0] = Math.max(minmax_data[0], ppp[2])
                minmax_data[1] = Math.min(minmax_data[1], ppp[2])
                return ppp
            })


        }




        // console.log("data", data)
        console.log("minmax_data", minmax_data)



        var min_size = Math.min(window.innerWidth, window.innerHeight)

        var margin = { top: 80, right: 25, bottom: 30, left: 40 },
            width = min_size - margin.left - margin.right,
            height = min_size - margin.top - margin.bottom;

        if (!this.heat_map_svg)
            this.heat_map_svg = d3.select('body').append('svg')

        this.heat_map_svg.selectAll("*").remove();

        var heat_map_ref = this.heat_map_svg
            .attr("width", min_size)
            .attr("height", min_size)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")")
        // .attr("viewBox", [0, 0, view_width, view_height]);



        this.tooltip_div.attr("class", "tooltip")
            .style("opacity", 0);


        // function onlyUnique(value, index, self) {
        //     return self.indexOf(value) === index;
        // }
        // Labels of row and columns -> unique identifier of the column called 'group' and 'variable'
        // var myGroups = data.map( function (d) { return d[0]; }).sort()
        // var myVars = data.map( function (d) { return d[1]; }).sort()

        var myGroups = d3.map(data, function (d) { return d[0]; }).keys().sort(function (a, b) { return parseInt(a, 10) - parseInt(b, 10) });
        var myVars = d3.map(data, function (d) { return d[1]; }).keys().sort(function (a, b) { return parseInt(a, 10) - parseInt(b, 10) });

        // var myGroups = d3.map(data, function (d) { return d[0]; }).keys().map(d => parseInt(d, 10)).sort()
        // var myVars = d3.map(data, function (d) { return d[1]; }).keys().map(d => parseInt(d, 10)).sort()

        // var myGroups = data.map(function (d) { return d[0]; })
        // myGroups = [...new Set(myGroups)].sort()
        // var myVars = data.map(function (d) { return d[1]; })
        // myVars = [...new Set(myVars)].sort()

        // console.log("myGroups", myGroups)
        // console.log("myVars", myVars)


        // Build X scales and axis:
        var x = d3.scaleBand()
            .range([0, width])
            .domain(myGroups)
            .padding(0.05);

        heat_map_ref.append("g")
            .style("font-size", 10)
            .attr("transform", "translate(0," + (height) + ")")
            .call(d3.axisBottom(x).tickSize(0))
            // .select(".domain").remove()
            .selectAll("text")
            .style("text-anchor", "end")
            .attr("dx", "-.8em")
            .attr("dy", ".15em")
            .attr("transform", "rotate(-80)")

        // Build Y scales and axis:
        var y = d3.scaleBand()
            .range([height, 0])
            .domain(myVars)
            .padding(0.05);


        heat_map_ref.append("g")
            .style("font-size", 10)
            // .attr("transform", "translate("+10+"," + 0 + ")")
            .call(d3.axisLeft(y).tickSize(0))
            .select(".domain").remove()

        // Build color scale
        var myColor = d3.scaleSequential(d3.interpolateInferno)
            .domain(minmax_data)

        // create a tooltip
        // var tooltip = d3.select("body")
        //     // .select("#my_dataviz")
        //     .append("div")
        //     .style("opacity", 0)
        //     .attr("class", "tooltip")
        //     .style("background-color", "white")
        //     .style("border", "solid")
        //     .style("border-width", "2px")
        //     .style("border-radius", "5px")
        //     .style("padding", "5px")

        // Three function that change the tooltip when user hover / move / leave a cell
        var mouseover = (d) => {
            this.tooltip_div
                .style("opacity", .9)
                .html("Cost of going from<br>" + d[0] + " to " + d[1] + "<br>this cell is: " + d[2])
                .style("left", (d3.event.pageX + 30) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        }
        var mouseleave = (d) => {

            this.tooltip_div
                .style("opacity", 0);

        }

        heat_map_ref.selectAll()
            .data(data, function (d) { return d[0] + ':' + d[1]; })
            .enter()
            .append("rect")
            .attr("x", function (d) { return x(d[0]) })
            .attr("y", function (d) { return y(d[1]) })
            .attr("rx", 4)
            .attr("ry", 4)
            .attr("width", x.bandwidth())
            .attr("height", y.bandwidth())
            .style("fill", function (d) { return myColor(d[2]) })
            .style("stroke-width", 4)
            .style("stroke", "none")
            // .style("opacity", 1)
            .on("mouseover", mouseover)
            .on("mouseleave", mouseleave)



        // Add title to graph
        heat_map_ref.append("text")
            .attr("x", 0)
            .attr("y", -50)
            .attr("text-anchor", "left")
            .style("font-size", "22px")
            .text("World elevation costs");

        // Add subtitle to graph
        heat_map_ref.append("text")
            .attr("x", 0)
            .attr("y", -20)
            .attr("text-anchor", "left")
            .style("font-size", "14px")
            .style("fill", "grey")
            .style("max-width", 400)
            .text("Costs of walking from one elevation to another.");



    }



}