

import * as dat from "dat.gui";
import { MapCity } from "./MapCity";












// // // var gui = new dat.GUI({ width: 400 });
// // var gui = new dat.GUI();

// class CityMapGui {

//     gui = new dat.GUI();


//     contr_cells_layout: any
//     contr_cells_count: any
//     contr_elev_cost_cutoff: any
//     contr_noise_height: any
//     contr_elevation_steps: any
//     contr_noise_seed: any
//     contr_noise_translate: any
//     contr_is_painting_elev: any
//     contr_elev_paint_height: any
//     contr_generate_map: any
//     contr_show_elev_cost_heatmap: any
//     contr_max_clicked_points: any

//     constructor(public map: MapCity) {


//         this.contr_cells_layout = this.gui.add(this.map, 'cells_layout', this.map.layout_opts);
//         this.contr_cells_count = this.gui.add(this.map, 'cells_count', 1, 10000, 10);
//         this.contr_elev_cost_cutoff = this.gui.add(this.map, 'elev_cost_cutoff', 1, 1000, 10);
//         this.contr_noise_height = this.gui.add(this.map, 'noise_spread', 0, 100, 0.01);
//         this.contr_elevation_steps = this.gui.add(this.map, 'elevation_steps', 0, 50, 1);
//         this.contr_noise_seed = this.gui.add(this.map, 'noise_seed', 0, 1, 0.01);
//         this.contr_noise_translate = this.gui.add(this.map, 'noise_translate', 0, 100, 0.1);
//         this.contr_is_painting_elev = this.gui.add(this.map, 'is_painting_elev');
//         this.contr_elev_paint_height = this.gui.add(this.map, 'elev_paint_height', this.map.elevation_val_minmax[0], this.map.elevation_val_minmax[1], 1);
//         this.contr_generate_map = this.gui.add(this.map, 'generate_map');
//         this.contr_show_elev_cost_heatmap = this.gui.add(this.map, 'show_elev_cost_heatmap');
//         this.contr_max_clicked_points = this.gui.add(this.map, 'max_clicked_points');

//     }


//     public update_elev_bounds = () => {
//         this.contr_elev_paint_height.min(this.map.elevation_val_minmax[0]).max(this.map.elevation_val_minmax[1]).step(1).setValue(this.map.elev_paint_height)

//         this.map.generate_map()
//         // calculate_map(map_set)
//     }


//     update_gui = () => {
//         if (!this.gui) return

//         for (var i in this.gui.__controllers) {
//             this.gui.__controllers[i].updateDisplay();
//             this.gui.__controllers[i].onChange(this.map.generate_map)
//         }

//         this.contr_elev_cost_cutoff.onChange(this.map.func_calc_map)
//         this.contr_is_painting_elev.onChange(this.map.check_painting)
//         this.contr_elev_paint_height.onChange(null)
//         this.contr_generate_map.onChange(null)

//         this.contr_max_clicked_points.onChange(this.map.func_calc_map)

//         this.contr_elevation_steps.onChange(this.update_elev_bounds)
//     }





// }



export function start_city() {
    console.log("start_city");


    var map_ = new MapCity()
    // var gui_ = new CityMapGui(map_)
    // gui_.update_gui()
    // gui_.gui.close()
    map_.draw()

}

