
import { MapBase } from "./map";

import * as d3 from "d3";
import * as d3g from "d3-geo-voronoi";
import * as d3d from "d3-delaunay";
import * as d3t from "d3-tricontour";

// var d3 = require("d3", "d3-geo-voronoi")

import { FlatTerrain, Cell } from "./flat_terrain"

import { FlatRoutes, FlatRoutesDraw } from "./flat_routes"

import * as dat from "dat.gui";

import * as noise_lib from 'noisejs';

import * as dju from "../libs/dij_utils.js";

export class MapCity extends MapBase {


    is_mouse_down = false

    delaunay: any
    voronoi: any
    find_pt: any
    contr_elev_paint_height: any

    terrain: FlatTerrain



    layout_opts: any = ["poisson", "hex", "grid", "normal", "random"]
    cells_layout: any = this.layout_opts[0]

    cells_count: any = 500
    // cells_count: any = 1000

    elev_cost_cutoff: any = 99999;
    elevation_steps: any = 10
    is_painting_elev: any = false
    elev_paint_height: any = 0


    selected_points: any
    draw_some_lines = []
    draw_some_points = []
    draw_some_text = []
    geo_dij_contours = []


    max_clicked_points = 2
    clicked_points = Array.from({ length: this.max_clicked_points + 1 }, () => (Math.random() * this.cells_count) | 0)


    d3_line = d3.line();
    // d3_line = d3.curveBasisClosed();



    // heat_map_svg = d3.select('body').append('svg')
    tooltip_div = d3.select("body").append("div")
    world_map_svg = d3.select('body').append('svg')


    polygons: any = this.world_map_svg.append("g").selectAll("*")
    contour_data: any = this.world_map_svg.append("g").selectAll("*")
    gr_conns2: any = this.world_map_svg.append("g").selectAll("*")
    points_1: any = this.world_map_svg.append("g").selectAll("*")
    render_1: any = this.world_map_svg.append("path")
    text_1: any = this.world_map_svg.append("g").selectAll("*")

    world_graph: any = null



    debug_map_svg: any = d3.select('body').append('svg')
    deb_polygons: any = this.debug_map_svg.append("g").selectAll("*")
    gr_conns1: any = this.debug_map_svg.append("g").selectAll("*")

    rivers = this.debug_map_svg.append("g").selectAll("*")
    rivers_w = this.world_map_svg.append("g").selectAll("*")
    river_paths: any = []




    routing = new FlatRoutes()
    routing_draw: FlatRoutesDraw



    constructor() {
        super()

        // this.d3_line.curve(d3.curveBasisClosed);

        this.resize()

        d3.select(window).on("resize", this.resize_view)
        this.world_map_svg.on("click", this.clicked)
        this.debug_map_svg.on("click", this.clicked)

        this.terrain = new FlatTerrain()

        this.terrain.set_size(this.view_width, this.view_height)

        this.terrain.generate_terrain(this)

        this.routing.addJoint(this.view_width / 2, this.view_height / 2)

        this.routing_draw = this.routing.addDrawSvg(this.world_map_svg)

        // this.check_painting()
    }



    public empty_lists() {
        this.draw_some_lines = []
        this.draw_some_points = []
        this.draw_some_text = []
        this.geo_dij_contours = []

    }


    public distance = (a, b) => {
        return Math.hypot(a[0] - b[0], a[1] - b[1]);
    }


    public resize = () => {
        this.view_width = window.innerWidth - 50;
        this.view_height = window.innerHeight - 50;

        this?.world_map_svg
            .attr("width", this.view_width)
            .attr("height", this.view_height)
        // .attr("viewBox", "-50 -50 " + (this.view_width+50) + " " + (this.view_height+50))


        this?.debug_map_svg
            .attr("width", this.view_width)
            .attr("height", this.view_height)

    }





    // public has_a_z_vec = (pt_, ind_) => {
    //     var v0 = this.voronoi.vectors[(ind_ * 4) + 0]
    //     var v1 = this.voronoi.vectors[(ind_ * 4) + 1]
    //     var v2 = this.voronoi.vectors[(ind_ * 4) + 2]
    //     var v3 = this.voronoi.vectors[(ind_ * 4) + 3]

    //     // var v0 = this.voronoi.vectors[(pt_.properties.id * 4) + 0]
    //     // var v1 = this.voronoi.vectors[(pt_.properties.id * 4) + 1]
    //     // var v2 = this.voronoi.vectors[(pt_.properties.id * 4) + 2]
    //     // var v3 = this.voronoi.vectors[(pt_.properties.id * 4) + 3]

    //     return v0 != 0 || v1 != 0 || v2 != 0 || v3 != 0
    // }





    // public get_graph_cell_elevation_edged = (p1_, p2_) => {
    //     var [cost_1_, cost_2_] = this.get_graph_cell_elevation(p1_, p2_)

    //     if (p2_?.properties?.is_edge === true && p1_?.properties?.is_edge === true) {
    //         cost_1_ += 1000
    //         cost_2_ += 1000
    //     }
    //     return [cost_1_, cost_2_]
    // }








    elevation_val_minmax = [0, 0]







    public get_minmax_color_gradient = (min: number, max: number) => {
        // https://cssgradient.io/

        var rel_sc = d3.scaleLinear().domain([0, 100]).range([min, max])
        var sc_data = [
            ["rgba(23,23,193, 1)", rel_sc(0)],
            ["rgba(49,117,223,1)", rel_sc(15)],
            ["rgba(31,182,46, 1)", rel_sc(20)],
            ["rgba(34,126,26, 1)", rel_sc(90)],
        ]

        return d3
            .scaleLinear()
            .domain(sc_data.map(d => { return <number>d[1] }))
            .range(sc_data.map(d => { return <number>d[0] }))
    }




    public get_as_coord_points = (cell_pts_) => {
        return cell_pts_.map(pt_ => pt_.coordinates)
    }


    public resize_view = () => {
        this.resize()

        // this.empty_lists()
        // var redo_pts_ = this.get_as_coord_points(this.points_obj)
        // // redo_pts_ = this.calc_cell_points(redo_pts_)
        // // this.points_obj = this.calculate_map(redo_pts_)

        this.draw()
    }

    public func_calc_map = () => {
        this.resize_view()
    }









    // public mark_edge_points = (points_) => {



    //     var edg_pts = []

    //     const { points, halfedges, triangles } = this.delaunay;
    //     for (let i = 0, j = 0, n = triangles.length, x, y; i < n; i += 3, j += 2) {
    //         const t1 = triangles[i] * 2;
    //         const t2 = triangles[i + 1] * 2;
    //         const t3 = triangles[i + 2] * 2;
    //         const x1 = points[t1];
    //         const y1 = points[t1 + 1];
    //         const x2 = points[t2];
    //         const y2 = points[t2 + 1];
    //         const x3 = points[t3];
    //         const y3 = points[t3 + 1];

    //         const dx = x2 - x1;
    //         const dy = y2 - y1;
    //         const ex = x3 - x1;
    //         const ey = y3 - y1;
    //         const bl = dx * dx + dy * dy;
    //         const cl = ex * ex + ey * ey;
    //         const ab = (dx * ey - dy * ex) * 2;

    //         if (!ab) {
    //             // degenerate case (collinear diagram)
    //             x = (x1 + x3) / 2 - 1e8 * ey;
    //             y = (y1 + y3) / 2 + 1e8 * ex;
    //         }
    //         else if (Math.abs(ab) < 1e-8) {
    //             // almost equal points (degenerate triangle)
    //             x = (x1 + x3) / 2;
    //             y = (y1 + y3) / 2;
    //         } else {
    //             const d = 1 / ab;
    //             x = x1 + (ey * bl - dy * cl) * d;
    //             y = y1 + (dx * cl - ex * bl) * d;
    //         }
    //         var pa = x;
    //         var pb = y;


    //         if (pa <= this.voronoi.xmin || pa >= this.voronoi.xmax || pb <= this.voronoi.ymin || pb >= this.voronoi.ymax) {

    //             edg_pts.push(t1)
    //             edg_pts.push(t2)
    //             edg_pts.push(t3)

    //             // if (pa <= this.voronoi.xmax) pa = this.voronoi.xmax
    //             // if (pb <= this.voronoi.xmin) pb = this.voronoi.xmin
    //             // if (pa >= this.voronoi.ymax) pa = this.voronoi.ymax
    //             // if (pb >= this.voronoi.ymin) pb = this.voronoi.ymin

    //             // this.draw_some_points.push([pa, pb])
    //         }
    //     }



    //     // console.log("edg_pts", edg_pts);
    //     edg_pts = [...new Set(edg_pts)];
    //     // console.log("edg_pts", edg_pts);

    //     for (let index = 0; index < edg_pts.length; index += 1) {
    //         var pt_ = Math.floor(edg_pts[index] / 2)

    //         points_[pt_].properties.is_edge = true

    //         // var pa_q = this.delaunay.points[(pt_ * 2) + 0] // + (Math.random() * 20) - 10
    //         // var pb_q = this.delaunay.points[(pt_ * 2) + 1] // + (Math.random() * 20) - 10
    //         // this.draw_some_points.push([pa_q, pb_q])
    //         // this.draw_some_text.push([pa_q, pb_q, pt_])

    //     }
    //     // console.log("this.draw_some_points.length",this.draw_some_points.length);

    // }



    public misc_debuggs = (points_) => {


        // for (const cell of this.voronoi.cellPolygons()) {
        //     console.log("cell ", cell );
        // }

        // console.log("this.delaunay", this.delaunay);
        // console.log("this.voronoi", this.voronoi);
        // console.log("this.voronoi", this.voronoi.cellPolygon(5));



        // console.log("this.points_obj.map((p)=>{ return p.elevation })", this.points_obj.map((p) => { return p.elevation }));



        // for (let index = 0; index < this.voronoi.vectors.length; index+=4) {
        //     var pa = this.voronoi.vectors[index+0] + (Math.random() * 30) - 15
        //     var pb = this.voronoi.vectors[index+1] + (Math.random() * 30) - 15
        //     var pa2 = this.voronoi.vectors[index+2] + (Math.random() * 30) - 15
        //     var pb2 = this.voronoi.vectors[index+3] + (Math.random() * 30) - 15
        //     if(pa == 0 || pa2 == 0) continue
        //     this.draw_some_lines.push([[pa, pb], [pa2, pb2]])
        // }



        // for (let index = 0; index < this.delaunay.points.length; index += 2) {
        //     var pa = this.delaunay.points[index + 0] + (Math.random() * 20) - 10
        //     var pb = this.delaunay.points[index + 1] + (Math.random() * 20) - 10
        //     this.draw_some_points.push([pa, pb])
        // }
        // console.log("this.draw_some_points.length",this.draw_some_points.length);

        // var out_cir = []
        // for (let index = 0; index < this.voronoi.circumcenters.length; index += 2) {
        //     var pa = this.voronoi.circumcenters[index + 0] // + (Math.random() * 20) - 10
        //     var pb = this.voronoi.circumcenters[index + 1] // + (Math.random() * 20) - 10

        // }
        // console.log("this.draw_some_points.length", this.draw_some_points.length);



        // const { points, halfedges, triangles } = this.delaunay;
        // for (let i = 0, n = halfedges.length; i < n; ++i) {
        //     const j = halfedges[i];
        //     if (j < i) continue;

        //     const ti = triangles[i];
        //     const tj = triangles[j];
        //     //   context.moveTo(points[ti * 2], points[ti * 2 + 1]);
        //     //   context.lineTo(points[tj * 2], points[tj * 2 + 1]);

        //     if (ti < 0 || tj < 0){
        //         console.log("i,j,   0  , ti,tj", i,j,   0  , ti,tj);
        //     }

        //     var ln = [
        //         [points[ti * 2], points[ti * 2 + 1]],
        //         [points[tj * 2], points[tj * 2 + 1]]
        //     ]

        //     if (ln[0][0] != null && ln[0][1] != null && ln[1][0] != null && ln[1][1]) {
        //         ln[0][0] += (Math.random() * 20) - 10
        //         ln[0][1] += (Math.random() * 20) - 10
        //         ln[1][0] += (Math.random() * 20) - 10
        //         ln[1][1] += (Math.random() * 20) - 10

        //         this.draw_some_points = this.draw_some_points.concat(ln)
        //         this.draw_some_lines.push(ln)
        //     }
        // }


        // var pts_ = this.delaunay.points
        // for (let index = 0; index < this.delaunay.triangles.length; index += 3) {
        //     const t0 = this.delaunay.triangles[index * 3 + 0];
        //     const t1 = this.delaunay.triangles[index * 3 + 1];
        //     const t2 = this.delaunay.triangles[index * 3 + 2];
        //     // console.log("index, t0,t1,t2", index, t0,t1,t2);
        //     this.draw_some_points.push([pts_[t0 * 2 + 0] + (Math.random() * 30) - 15, pts_[t0 * 2 + 1] + (Math.random() * 30) - 15])
        //     this.draw_some_points.push([pts_[t1 * 2 + 0] + (Math.random() * 30) - 15, pts_[t1 * 2 + 1] + (Math.random() * 30) - 15])
        //     this.draw_some_points.push([pts_[t2 * 2 + 0] + (Math.random() * 30) - 15, pts_[t2 * 2 + 1] + (Math.random() * 30) - 15])
        //     // var pa = this.voronoi.vectors[index+0] + (Math.random() * 30) - 15
        //     // var pb = this.voronoi.vectors[index+1] + (Math.random() * 30) - 15
        //     // var pa2 = this.voronoi.vectors[index+2] + (Math.random() * 30) - 15
        //     // var pb2 = this.voronoi.vectors[index+3] + (Math.random() * 30) - 15
        //     // if(pa == 0 || pa2 == 0) continue
        //     // this.draw_some_lines.push([[pa, pb], [pa2, pb2]])
        // }


        // for (let index = 0; index < this.points_obj.length; index += 1) {
        //     var pt_ = this.points_obj[index]

        //     if (this.has_a_z_vec(pt_,index)) {
        //         console.log("p1_", pt_);

        //         var pa0 = this.delaunay.points[(index*2) + 0] + (Math.random() * 20) - 10
        //         var pb0 = this.delaunay.points[(index*2) + 1] + (Math.random() * 20) - 10
        //         this.draw_some_points.push([pa0, pb0])


        //         var [pa, pb] = pt_.coordinates
        //         pa += this.voronoi.vectors[(index * 4) + 0]
        //         pb += this.voronoi.vectors[(index * 4) + 1]
        //         var [pa2, pb2] = pt_.coordinates
        //         pa2 += this.voronoi.vectors[(index * 4) + 2]
        //         pb2 += this.voronoi.vectors[(index * 4) + 3]

        //         this.draw_some_lines.push([[pa, pb], [pa2, pb2]])

        //     }
        // }




    }


    public draw = () => {

        // console.log("full_polys", full_polys);
        // console.log("this.voronoi.cellPolygons()", this.voronoi.cellPolygons());

        // for (const cell of this.voronoi.cellPolygons()) {
        //     // console.log("cell ", cell );
        //     full_polys.push(cell)
        // }

        // console.log("this.terrain.points_obj", this.terrain.points_obj);

        // var full_polys = [...this.voronoi.cellPolygons()]
        // console.log("full_polys",full_polys);
        // this.polygons = this.polygons.data(full_polys, this.get_poly_id)
        this.polygons = this.polygons.data(this.terrain.points_obj, this.get_poly_id)
            .join(
                enter => enter.append("path")
                    .call(this.update_polygon)
                // .style("stroke", "#A00")
                ,
                update => update
                    .call(this.update_polygon),
                exit => exit.remove()
            )


        this.deb_polygons = this.deb_polygons.data(this.terrain.points_obj, this.get_poly_id)
            .join(
                enter => enter.append("path")
                    .call(this.update_polygon_deb)
                // .style("stroke", "#A00")
                ,
                update => update
                    .call(this.update_polygon_deb),
                exit => exit.remove()
            )




        // this.render_1 = this.render_1
        //     .attr("fill", "none")
        //     .attr("stroke", "#ccc")
        //     .attr("stroke-width", 1)
        //     // .attr("d", this.voronoi.delaunay.renderHull())
        //     // .attr("d", this.voronoi.delaunay.renderPoints())
        //     .attr("d", this.voronoi.render())
        // // .attr("d", aaaa)




        this.contour_data = this.contour_data.data(this.geo_dij_contours)
            .join(
                enter => enter.append("path")
                    .style("stroke", "#111")
                    .attr('stroke-width', "3px")
                    .attr("fill", "rgba(0,0,0,0)")
                    // .attr("fill", color)
                    .attr("d", d3.geoPath()),
                update => update.attr("d", d3.geoPath()),
                exit => exit.remove()
            )







        // this.gr_conns1 = this.gr_conns1.data(this.world_graph.listed, this.get_link_1_id)
        //     .join(
        //         enter => enter
        //             .append("line")
        //             .style("stroke", "black")
        //             .attr('stroke-width', "1px")
        //             .attr("x1", link_ => { return this.points_obj[link_[0]].coordinates[0] })
        //             .attr("y1", link_ => { return this.points_obj[link_[0]].coordinates[1] })
        //             .attr("x2", link_ => { return this.points_obj[link_[1]].coordinates[0] })
        //             .attr("y2", link_ => { return this.points_obj[link_[1]].coordinates[1] })
        //         ,
        //         update => update
        //             .attr("x1", link_ => { return this.points_obj[link_[0]].coordinates[0] })
        //             .attr("y1", link_ => { return this.points_obj[link_[0]].coordinates[1] })
        //             .attr("x2", link_ => { return this.points_obj[link_[1]].coordinates[0] })
        //             .attr("y2", link_ => { return this.points_obj[link_[1]].coordinates[1] })
        //         ,
        //         exit => exit.remove()
        //     )





        // var aaaa = [...this.voronoi.delaunay.trianglePolygons()]
        // console.log("aaaa", aaaa);
        // lines = lines.concat(aaaa)

        var lines = []
        lines = lines.concat(this.draw_some_lines)
        this.gr_conns2 = this.gr_conns2.data(lines, this.get_link_1_id)
            .join(
                enter => enter
                    .append("path")
                    .style("opacity", 0.7)
                    .attr("fill", "rgba(0,0,0,0)")
                    // .attr("stroke", function (d, i) { return d3.schemeDark2[i % 10] })
                    .style("stroke", "red")
                    .attr('stroke-width', "3px")
                    .attr("d", this.d3_line)
                ,
                update => update
                    .attr("d", this.d3_line)
                ,
                exit => exit.remove()
            )

        this.points_1 = this.points_1.data(this.draw_some_points, this.get_link_1_id)
            .join(
                enter => enter
                    .append("circle")
                    .style("opacity", 0.3)
                    .style("r", 5)
                    .attr("fill", "yellow")
                    .attr("cx", function (d) { return d[0]; })
                    .attr("cy", function (d) { return d[1]; })
                ,
                update => update
                    .attr("d", this.d3_line)
                ,
                exit => exit.remove()
            )




        this.text_1 = this.text_1.data(this.draw_some_text, this.get_link_1_id)
            .join(
                enter => enter
                    .append("text")
                    // .style("opacity", 0.3)
                    // .style("r", 5)
                    .attr("fill", "black")
                    .attr("x", function (d) { return d[0]; })
                    .attr("y", function (d) { return d[1]; })
                    .html(function (d) { return d[2]; })
                ,
                update => update
                    .attr("d", this.d3_line)
                ,
                exit => exit.remove()
            )




        // this.rivers_w = this.rivers_w.data(this.river_paths, this.get_link_1_id)
        //     .join(
        //         enter => enter
        //             .append("path")
        //             .style("opacity", 0.7)
        //             .attr("fill", "rgba(0,0,0,0)")
        //             .attr("stroke", function (d, i) { return d3.schemeDark2[i % 10] })
        //             // .style("stroke", "blue")
        //             .attr('stroke-width', "8px")
        //             .attr("d", this.d3_line)
        //         ,
        //         update => update
        //             .attr("d", this.d3_line)
        //         ,
        //         exit => exit.remove()
        //     )


        this.rivers = this.rivers.data(this.river_paths, this.get_link_1_id)
            .join(
                enter => enter
                    .append("path")
                    .style("opacity", 0.7)
                    .attr("fill", "rgba(0,0,0,0)")
                    .attr("stroke", function (d, i) { return d3.schemeDark2[i % 10] })
                    // .style("stroke", "blue")
                    .attr('stroke-width', "8px")
                    .attr("d", this.d3_line)
                ,
                update => update
                    .attr("d", this.d3_line)
                ,
                exit => exit.remove()
            )

        this.routing_draw.draw()

    }





    // var all_pths = []
    // var geo_dij_contours = []
    // if (this.world_graph) {


    //     var vals_ids = Array.from({ length: 5 }, () => (Math.random() * this.points_obj.length) | 0)
    //     var vals_pts = vals_ids.map((id_) => this.points_obj[id_])
    //     geo_dij_contours = geo_dij_contours.concat(vals_pts)
    //     // console.log("vals_ids", vals_ids)

    //     var tree = dju.shortest_tree({
    //         graph: this.world_graph,
    //         origins: vals_ids,
    //         cutoff: this.elev_cost_cutoff,
    //     })
    //     var tree_fin = [...tree][0]
    //     // console.log("tree_fin", tree_fin);


    //     var path = dju.shortest_paths(this.world_graph, tree_fin)
    //     console.log("!!!!!!!!!! path", path);

    //     path.forEach(pth_ => {
    //         var pth_lines = []
    //         pth_.path.forEach((id_, inx_, arr) => {

    //             var [pa, pb] = this.points_obj[id_].coordinates
    //             pa += (Math.random() * 20) - 10
    //             pb += (Math.random() * 20) - 10

    //             pth_lines.push([pa, pb])

    //             // // if (arr.length == inx_ - 1) return;
    //             // if (id_ && arr[inx_ + 1]) {
    //             //     // pth_lines.push([id_, arr[inx_ + 1]])
    //             //     var [pa, pb] = this.points_obj[id_].coordinates
    //             //     pa += (Math.random() * 20) - 10
    //             //     pb += (Math.random() * 20) - 10

    //             //     pth_lines.push([pa, pb])
    //             //     console.log("id_, inx_",id_, inx_, arr[inx_ + 1]);
    //             // }else {
    //             // console.log(" id_, inx_, arr",id_, inx_, arr,arr[inx_ + 1]);
    //             // }
    //         });
    //         all_pths.push(pth_lines)
    //     });
    //     // console.log("pth_lines", pth_lines);
    //     console.log("all_pths", all_pths);

    //     var cont_points = this.points_obj.map(function (pts_) {
    //         return [].concat(pts_.coordinates)
    //     })

    //     for (let i = 0; i < vals_ids.length; i++) {

    //         var pt_cont = d3t.tricontour()
    //             .value((d, j) => (tree_fin.origin[j] === vals_ids[i] ? 1 : 0))
    //             .contour(cont_points, 0.6)

    //         // contour_points_feat = ctr_(cont_points, 0.6)
    //         // geo_dij_contours = ctr_(cont_points, 0.6)
    //         // var pt_cont = ctr_(cont_points, 0.6)
    //         geo_dij_contours.push(pt_cont)

    //         // console.log("cont_points", cont_points)
    //         // console.log("contour_points_feat", contour_points_feat)
    //         // console.log("pt_cont", pt_cont)
    //     }
    //     console.log("geo_dij_contours", geo_dij_contours)

    // }




    private get_color_elev = (d, i) => {
        return d3.scaleLinear<string>()
            .domain([this.terrain.elevation_val_minmax[0], 0, this.terrain.elevation_val_minmax[1]])
            .range(["rgba(23,23,193, 1)", "lightgray", "green"])
            (d.elevation)

        // return loc_col
    }



    // private get_color_riv = (d, i) => {
    //     return d3.scaleLinear<string>()
    //         .domain([this.river_val_minmax[0], 0, this.river_val_minmax[1]])
    //         .range(["rgba(23,23,193, 1)", "lightgray", "green"])
    //         (d.properties.river_val)

    //     // return loc_col
    // }



    public update_polygon = (poly_) => {
        // console.log("poly_", poly_);
        poly_
            // .attr("d", this.d3_line )
            .attr("d", (d, i) => {
                if (d?.poly == undefined) {
                    console.log("d", d);
                    return null
                }
                // console.log("d.poly", d.poly);
                return this.d3_line(d.poly)
            })
            .style("stroke", "#000")
            // .attr("fill", "#f4f4f4")
            // .style("opacity", 0.7)
            .attr("fill", this.get_color_elev)
    }


    public update_polygon_deb = (poly_) => {
        poly_
            // .attr("d", this.d3_line )
            .attr("d", (d, i) => {
                if (d?.poly == null)
                    return null
                return this.d3_line(d.poly)
            })
            // .attr("fill", "#f4f4f4")
            // .attr("stroke", this.get_color_elev)
            // .style("stroke", "#000")
            // .attr('stroke-width', "10px")
            // .attr("stroke", this.get_color_elev)
            .attr("fill", this.get_color_elev)
    }



    private get_link_1_id = (params) => {
        // console.log(params)
        return "" + params[0] + ":" + params[1]
    }

    public get_poly_id = (params, i) => {
        var pt_ = this.terrain.points_obj[i]
        // if (!pt_?.properties?.id)
        //     console.log("params", i, this.points_obj.length, params, pt_);
        // console.log("pt_", pt_,params);
        // return pt_.properties.id
        return pt_?.id
    }


    public clicked = (d, i, n) => {
        // console.log("CLICK ...", d3.event)
        // var this_ = this;


        if (d3.event.shiftKey) {
            var mous_ = d3.mouse(n[i])

            console.log("mous_[0], mous_[1]", mous_[0], mous_[1]);
            this.find_pt = this.delaunay.find(mous_[0], mous_[1], this.find_pt);
            // console.log("this.points_obj[this.find_pt]", this.find_pt, this.points_obj[this.find_pt]);

            console.log("this.points_obj[this.find_pt]", this.terrain.points_obj[this.find_pt]);

            // var all_neigs = [...this.delaunay.neighbors(this.find_pt)]
            var p1_neigs = this.terrain.true_neighbors(this.find_pt)
            console.log("p1_neigs", p1_neigs);


        } else if (d3.event.ctrlKey) {

            var mous_ = d3.mouse(n[i])
            this.find_pt = this.delaunay.find(mous_[0], mous_[1], this.find_pt);
            this.clicked_points.push(this.find_pt)

            while (this.clicked_points.length > this.max_clicked_points)
                this.clicked_points.shift()

            console.log("this.clicked_points", this.clicked_points);

            this.func_calc_map()

        }
    }


    public draw_paint_brush = (d, i, n) => {
        var mous_ = d3.mouse(n[i])
        // console.log("mous_", mous_);


        // var bsize = 80

        // console.log("this", this);
        // console.log("this.delaunay", this.delaunay);

        // this.find_pt = this.delaunay.find(mous_[0], mous_[1], this.find_pt);

        // this.selected_points = [this.find_pt]
        // // console.log("CLICK ...", d3.event)

        // if (this.is_mouse_down) {
        //     this.points_obj[this.find_pt].elevation = this.elev_paint_height
        // }

        // var delaunay = d3.Delaunay.from(data); // https://bl.ocks.org/Fil/3faaaf1b5f34b03a7a2235bf22e20b73
        // delaunay.findAll = function(x, y, radius) {
        //   const points = delaunay.points,
        //         results = [],
        //         seen = [],
        //         queue = [delaunay.find(x, y)];

        //   while (queue.length) {
        //     const q = queue.pop();
        //     if (seen[q]) continue;
        //     seen[q] = true;
        //     if (Math.hypot(x - points[2*q], y - points[2*q+1]) < radius) {
        //       results.push(q);
        //       for (const p of delaunay.neighbors(q)) queue.push(p);
        //     }
        //   }

        //   return results;
        // }







    }


    public mouseDown = () => {
        this.is_mouse_down = true
        // console.log("mouseDown");
    }

    public mouseUp = () => {
        this.is_mouse_down = false
        // console.log("mouseUp");
    }


    public check_painting = () => {
        // console.log("this.is_painting_elev", this.is_painting_elev);
        if (this.is_painting_elev) {
            this.world_map_svg.on("mousemove", this.draw_paint_brush)
                .on("mousedown", this.mouseDown)
                .on("mouseup", this.mouseUp)

        } else {
            this.world_map_svg.on("mousemove", null)
            // this.draw()
        }

    }







}