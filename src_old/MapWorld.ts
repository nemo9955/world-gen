

import { MapBase } from "./map";

import * as d3 from "d3";
import * as d3g from "d3-geo-voronoi";

// var d3 = require("d3", "d3-geo-voronoi")


import * as dat from "dat.gui";

import * as noise_lib from 'noisejs';

import * as dju from "../libs/dij_utils.js";






export class MapWorld extends MapBase {



    aprox_points: any = 900
    generate_fib: any = true
    generate_rand: any = true
    round_coords: any = true
    show_points: any = false
    elev_cost_cutoff: any = 9999
    noise_height: any = 1
    world_elevation: any = 60
    noise_seed: any = Math.random()
    noise_translate: any = 100 + (Math.random() * 999)
    find_pt: any = null
    diagram: any = null



    is_mouse_down = false
    color: any = d3.scaleSequential(d3.interpolateRdBu).domain([300, -300])

    world_map_svg: any
    polygon: any
    contour_data: any
    some_links: any
    shape_point: any
    shape_centroids: any
    general_point: any
    general_points_list: any = {}
    contour_points_feat: any = {}
    initial_scale: any
    initial_rotate: any
    projection: any
    geoPath: any


    constructor() {
        super()


        this.view_width = window.innerWidth - 50;
        this.view_height = window.innerHeight - 200;

        this.world_map_svg = d3.select('body').append('svg')


        this.polygon = this.world_map_svg.append("g")
            .selectAll("path")

        this.contour_data = this.world_map_svg.append("g")
            .selectAll("path")

        this.some_links = this.world_map_svg.append('g')
            .attr('class', 'links')
            .selectAll('path')

        this.shape_point = this.world_map_svg.append("g")
            .selectAll("path.point");

        this.shape_centroids = this.world_map_svg.append("g")
            .selectAll("path.point");

        this.general_point = this.world_map_svg.append("g")
            .selectAll("path.point");



        this.initial_scale = 300;
        this.initial_rotate = {
            x: 0,
            y: 0
        };


        // this.projection = d3.geoOrthographic(); this.initial_scale = 300
        this.projection = d3.geoNaturalEarth1(); this.initial_scale = 150
        // this.projection = d3.geoMercator(); this.initial_scale = 140



        this.projection.scale(this.initial_scale)
            .translate([this.view_width / 2, this.view_height / 2])
            .rotate([this.initial_rotate.x, this.initial_rotate.y])
            .center([0, 0])

        this.geoPath = d3.geoPath()
            .projection(this.projection);


        this.geoPath.pointRadius(function (d: any) {
            // console.log(d)
            try {
                return d.properties.size;
            }
            catch (error) {
                return 5;
            }
        })



        this.world_map_svg.on("click", this.clicked)
        d3.select(window).on("resize", this.resize)
        d3.select("body").on("keypress", this.key_press)


        this.world_map_svg.call(d3.zoom().on('zoom', this.zoomed));

        // this.world_map_svg.on("mousemove", this.draw_paint_brush)
        //     .on("mousedown", this.mouseDown)
        //     .on("mouseup", this.mouseUp)



        this.initialize_map()
        this.resize()




    }


    public updated_geo_points = () => {

        var sphere_points = []
        var sphere_centroids = []

        if (this.show_points) {
            this.diagram.polygons().features.forEach(feat_ => {

                var coords_ = feat_.properties.site
                var centroid_ = d3.geoCentroid(feat_)
                // console.log("feat_:", feat_)
                // console.log("centroid_:", centroid_)

                sphere_points.push(coords_)
                // sphere_points.push({
                //     type: "Point",
                //     coordinates: coords_,
                //     properties: {
                //         id: coords_[0] + ":" + coords_[1]
                //     }
                // })


                sphere_centroids.push({
                    type: "Point",
                    coordinates: centroid_,
                    properties: {
                        id: "c" + coords_.properties.id,
                        size: 5,
                    }
                })

            });

        }

        this.shape_point = this.shape_point
            .data(sphere_points, this.get_point_id)
            .join(
                enter => enter
                    .append("path")
                    .attr("class", "point")
                    .attr("fill", "white")
                    .style("stroke", "blue")
                    .attr('stroke-width', "2px")
                    .attr("d", this.geoPath)
                ,
                update => update
                    .attr("d", this.geoPath)
                ,
                exit => exit.remove()
            )



        this.shape_centroids = this.shape_centroids
            .data(sphere_centroids, this.get_point_id)
            .join(
                enter => enter.append("path")
                    .attr("class", "point")
                    .attr("fill", "white")
                    .style("stroke", "red")
                    .attr('stroke-width', "2px")
                    .attr("d", this.geoPath),
                update => update.attr("d", this.geoPath),
                exit => exit.remove()
            )


        var vals = Object.keys(this.general_points_list).map(function (key) {
            return this.general_points_list[key];
        });

        this.general_point = this.general_point
            .data(vals, this.get_point_id)
            .join(
                enter => enter.append("path")
                    .attr("class", "point")
                    .attr("fill", "black")
                    .style("stroke", "green")
                    .attr('stroke-width', "4px")
                    .attr("d", this.geoPath),
                update => update.attr("d", this.geoPath),
                exit => exit.remove()
            )

    }


    public update_geo_mesh = () => {

        this.updated_geo_points()

        var geo_dij_contours = []

        if (this.world_graph) {

            var vals_ids = Object.keys(this.general_points_list).map(function (key) {
                return this.general_points_list[key].properties.id;
            });
            console.log("vals_ids", vals_ids)

            const run = dju.shortest_tree({
                origins: vals_ids,
                cutoff: this.elev_cost_cutoff,
                graph: this.world_graph
            }).next().value;

            // console.log("run.origin", run.origin)

            var cont_points = this.diagram._data.map(function (pts_) {
                // console.log("pts_", pts_)
                return [].concat(pts_.coordinates)
                // .concat([pts_.properties.elevation])
            })

            for (let i = 0; i < vals_ids.length; i++) {

                var pt_cont = d3g.geoContour()
                    // var ctr_ = d3.geoContour()
                    .value((d, j) => (run.origin[j] === vals_ids[i] ? 1 : 0))
                    // .thresholds(d3.range(-300, 300, 50))
                    // .thresholds(1)
                    .contour(cont_points, 0.6)

                // contour_points_feat = ctr_(cont_points, 0.6)
                // geo_dij_contours = ctr_(cont_points, 0.6)
                // var pt_cont = ctr_(cont_points, 0.6)
                geo_dij_contours.push(pt_cont)

                // console.log("cont_points", cont_points)
                // console.log("contour_points_feat", contour_points_feat)
                console.log("pt_cont", pt_cont)
                console.log("geo_dij_contours", geo_dij_contours)
            }

        }


        var cont_list = []
            .concat(geo_dij_contours)
        // .concat(contour_points_feat)
        console.log("cont_list", cont_list)
        // make_topo_map
        // polygon = polygon.data(diagram.triangles().features,get_poly_id)
        this.contour_data = this.contour_data.data(cont_list)
            .join(
                enter => enter.append("path")
                    .style("stroke", "#111")
                    .attr('stroke-width', "3px")
                    .attr("fill", "rgba(0,0,0,0)")
                    // .attr("fill", color)
                    .attr("d", this.geoPath),
                update => update.attr("d", this.geoPath),
                exit => exit.remove()
            )



        // console.log("diagram.polygons().features", diagram.polygons().features)
        // polygon = polygon.data(diagram.triangles().features,get_poly_id)
        this.polygon = this.polygon.data(this.diagram.polygons().features, this.get_poly_id)
            .join(
                enter => enter.append("path")
                    .attr("d", this.geoPath)
                    .call(this.update_polygon)
                // .style("stroke", "#A00")
                ,
                update => update.attr("d", this.geoPath)
                    .call(this.update_polygon),
                exit => exit.remove()
            )




        // some_links = some_links.data(diagram.links().features)
        //     .join(
        //         enter => enter.append("path")
        //             .attr("d", geoPath)
        //             .classed('secondary', function (d) {
        //                 return !d.properties.urquhart;
        //             }),
        //         update => update.attr("d", geoPath),
        //         exit => exit.remove()
        //     )






    }

    public get_point_id = (params) => {
        // console.log(params)
        return params.properties.id
    }

    public get_poly_id = (params) => {
        // console.log(params)
        return params.properties.site.properties.id
    }

    public componentToHex = (c) => {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }


    public rgba = (r, g, b, a) => {
        return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
    }


    public update_polygon = (poly_) => {
        var this_ = this;
        poly_
            .style("stroke", "#000")
            // .attr("fill", "#f4f4f4")
            .attr("fill", function (d) {
                // console.log("fill poly ... ", d.properties.site.properties.elevation, d)
                // return "#f4f4f4"


                var loc_col = this_.relief_gradient(d.properties.site.properties.elevation)
                var polsc = d3
                    // .scaleSqrt()
                    .scalePow<string>().exponent(0.2)
                    .domain([0, 60, 90])
                    .range([loc_col, loc_col, "white"])
                // .range(["white", loc_col])
                var fin_col = polsc(Math.abs(d.properties.sitecoordinates[1]))

                return fin_col
            })
    }



    public redraw = () => {
        this.world_map_svg.selectAll("path").attr("d", this.geoPath);
    }


    public clicked = (d, i, n) => {
        // console.log("CLICK ...", d3.event)
        if (d3.event.shiftKey) {
            this.findcell(this.projection.invert(d3.mouse(n[i])));
        }
        else if (d3.event.ctrlKey) {
            var m = this.projection.invert(d3.mouse(n[i]))

            var found = this.diagram.find(m[0], m[1], 0);
            // var cobj = polygon._groups[0][found]
            var cobj = this.diagram.polygons().features[found]
            var coid = this.get_poly_id(cobj)
            console.log("CLICKED ON : ", found, cobj)

            if (coid in this.general_points_list) {
                delete this.general_points_list[coid]
            } else {
                this.general_points_list[coid] = JSON.parse(JSON.stringify(cobj.properties.site))
                this.general_points_list[coid].properties.size = 8
                console.log("general_points_list[coid] : ", this.general_points_list[coid])
            }

            // console.log("POINTS ...", general_points_list)
            // updated_geo_points()
            this.update_geo_mesh()

        }
        else {

            // console.log("polygon", polygon);

        }
    }


    public mouseDown = () => {
        this.is_mouse_down = true
        // console.log("this", this);
        // console.log("this.is_mouse_down", this.is_mouse_down);
    }

    public mouseUp = () => {
        this.is_mouse_down = false
        // console.log("this", this);
        // console.log("this.is_mouse_down ", this.is_mouse_down);
    }



    public draw_paint_brush = (d, i, n) => {

        var m = this.projection.invert(d3.mouse(n[i]))


        // var bsize = 80


        // map_set.find_pt = map_set.delaunay.find(mous_[0], mous_[1], map_set.find_pt);

        // map_set.selected_points = [map_set.find_pt]
        // // console.log("CLICK ...", d3.event)


        // console.log("this.is_mouse_down", this.is_mouse_down);

        if (this.is_mouse_down) {
            // map_set.points_obj[map_set.find_pt].properties.elevation = map_set.elev_paint_height

            this.find_pt = this.diagram.find(m[0], m[1], this.find_pt);
            // console.log("find_pt", find_pt);

            var ceva = this.polygon._groups[0][this.find_pt]
            var d3ceva = d3.select(ceva)

            var pt_ = this.diagram.valid[this.find_pt]
            pt_.properties.elevation = 0

            // console.log("ceva", ceva);

            // console.log("d3ceva", d3ceva);

            d3ceva.call(this.update_polygon)



        }





    }


    public findcell = (m) => {
        console.log("m ..", m)
        this.polygon.call(this.update_polygon)
        var found = this.diagram.xfind(m[0], m[1], 50);
        if (found) {
            // console.log(found)
            console.log(found, found.index)
            var ceva = this.polygon._groups[0][found.index]
            // var ceva = polygon._groups[0][found.index]
            ceva.setAttribute('fill', 'red');
        }
    }


    public zoomed = () => {
        var transform = d3.event.transform;


        var window_yaw = d3.scaleLinear()
            .domain([-this.view_width, this.view_width])
            .range([-180, 180])

        var window_pitch = d3.scaleLinear()
            .domain([-this.view_height, this.view_height])
            .range([90, -90]);


        var r = {
            x: window_yaw(transform.x),
            y: window_pitch(transform.y)
        };
        // var k = Math.sqrt(100 / projection.scale());

        if (d3.event.sourceEvent.wheelDelta) {
            this.projection.scale(this.initial_scale * transform.k)
        } else {
            this.projection.rotate([this.initial_rotate.x + r.x, this.initial_rotate.y + r.y]);
        }

        this.redraw()
    };


    public resize = () => {
        this.view_width = window.innerWidth - 100;
        this.view_height = window.innerHeight - 200;

        // console.log("view_width", view_width, "view_height", view_height)

        if (this.world_map_svg)
            this.world_map_svg
                .attr("width", this.view_width)
                .attr("height", this.view_height);


        if (this.projection)
            this.projection
                // .scale(initial_scale)
                .translate([this.view_width / 2, this.view_height / 2])
                .center([0, 0])
        // .rotate([initial_rotate.x, initial_rotate.y])

        this.redraw()
    }





    public make_sphere_points_ = (number) => {
        return []
    }


    public make_sphere_points_fib_phi = (number) => {
        var phi = (1 + Math.sqrt(5)) / 2
        return Array.from({ length: number }, (_, i) => [
            i / phi * 360 % 360,
            Math.acos(2 * i / number - 1) / Math.PI * 180 - 90
        ])
    }

    public make_sphere_points_rand_wrong = (number) => {
        return d3.range(number)
            .map(function (d) { return [Math.random() * 360, Math.random() * 90 - Math.random() * 90]; });
    }


    public make_sphere_points_rand_unif = (number) => {
        const degrees = 180 / Math.PI
        return d3.range(number)
            .map(function (d) { return [Math.random() * 360, Math.acos(2 * Math.random() - 1) * degrees - 90]; });
    }







    public make_voronoi = (points) => {

        var diagram = d3g.geoVoronoi()(points);

        // this is a variant of diagram.find()
        // that colors the intermediate steps
        diagram.xfind = function (x, y, radius) {
            var features = diagram.polygons().features;

            // optimization: start from most recent result
            var i, next = diagram.find.found || 0;
            var cell = features[next] || features[next = 0];

            var dist = d3.geoLength({
                type: 'LineString',
                coordinates: [[x, y], cell.properties.sitecoordinates]
            });
            do {
                cell = features[i = next];
                cell.properties.index = i
                next = null;
                this.polygon._groups[0][i].setAttribute('fill', '#f5a61d');
                cell.properties.neighbours.forEach(function (e) {

                    if (this.polygon._groups[0][e].getAttribute('fill') != '#f5a61d')
                        this.polygon._groups[0][e].setAttribute('fill', '#fbe8ab');

                    var ndist = d3.geoLength({
                        type: 'LineString',
                        coordinates: [[x, y], features[e].properties.sitecoordinates]
                    });
                    if (ndist < dist) {
                        dist = ndist;
                        next = e;
                        return;
                    }
                });

            } while (next !== null);
            diagram.find.found = i;
            if (!radius || dist < radius * radius) {
                return cell.properties;
            }
        }

        return diagram

    }






    pi = Math.PI;
    halfPi = this.pi / 2;
    degrees = 180 / this.pi;
    radians = this.pi / 180;
    atan2 = Math.atan2;
    cos = Math.cos;
    max = Math.max;
    min = Math.min;
    sin = Math.sin;
    sign = Math.sign
    sqrt = Math.sqrt;

    public asin = (x) => {
        return x > 1 ? this.halfPi : x < -1 ? -this.halfPi : Math.asin(x);
    }

    // Converts 3D Cartesian to spherical coordinates (degrees).
    public spherical = (cartesian) => {
        return [
            this.atan2(cartesian[1], cartesian[0]) * this.degrees,
            this.asin(this.max(-1, this.min(1, cartesian[2]))) * this.degrees
        ];
    }

    // Converts spherical coordinates (degrees) to 3D Cartesian.
    public cartesian = (coordinates, radius = 1) => {
        var lambda = radius * coordinates[0] * this.radians,
            phi = radius * coordinates[1] * this.radians,
            cosphi = radius * this.cos(phi);
        return [cosphi * this.cos(lambda), cosphi * this.sin(lambda), this.sin(phi)];
    }











    public generate_some_points = () => {

        var generators = d3.sum([this.generate_fib, this.generate_rand])
        // console.log("generators", generators)
        if (generators <= 0) return null

        var pts_per_gen = this.aprox_points / generators
        console.log("Will gen aprox total ", this.aprox_points, " and per gen", pts_per_gen)
        var sites = []

        sites = sites.concat([[0, -90], [0, 0]])

        if (this.generate_fib)
            sites = sites.concat(this.make_sphere_points_fib_phi(pts_per_gen))

        if (this.generate_rand)
            sites = sites.concat(this.make_sphere_points_rand_unif(pts_per_gen))

        // sites = sites.concat(make_sphere_points_rand_wrong(pts_per_gen))

        if (this.round_coords) {
            sites.forEach(pt_ => {
                pt_[0] = Math.round(pt_[0])
                pt_[1] = Math.round(pt_[1])
            });

        }
        console.log("Total generated ", sites.length)

        // console.log("sites", sites.length, sites)
        // console.log("sites", sites.length ,sites.toLocaleString())

        return sites
    }


    // public rm_dup_points = (sites) => {


    //     sites.sort((a, b) => a[1] - b[1])
    //     sites.sort((a, b) => a[0] - b[0])

    //     // console.log("sites", sites.length ,sites.toLocaleString())

    //     for (let index = 0; index < sites.length; index++) {
    //         const pt_ = sites[index];

    //         // sites.forEach(function (pt_, i, sites) {
    //         if (index + 1 >= sites.length) continue;
    //         var pt2_ = sites[index + 1]

    //         if (pt_[0] === pt2_[0] && pt_[1] === pt2_[1]) {
    //             var len1 = sites.length
    //             var rmmm = sites.splice(index + 1, 1)
    //             var len2 = sites.length
    //             index--
    //             // console.log("dup", len1, len2, pt_, pt2_, "rmmm",rmmm.toLocaleString())
    //         }
    //     }
    //     // console.log("sites", sites.length ,sites.toLocaleString())

    // }



    public make_geo_points = (sites) => {
        var geo_sites = sites.map(function (pt_, index) {
            return {
                type: "Point",
                coordinates: pt_,
                properties: {
                    id: index,
                    size: 5
                }
            }
        })

        return geo_sites
    }

    public geopt_add_perlin_height = (sites) => {

        var noise = new noise_lib.Noise(this.noise_seed);
        console.log("noise_seed", this.noise_seed, "noise_translate", this.noise_translate)
        noise.seed(this.noise_seed);

        this.world_elevation_min = null
        this.world_elevation_max = null

        sites.forEach((point_obj, index) => {
            var pt_ = point_obj.coordinates
            var cco = this.cartesian([pt_[0], pt_[1]], this.noise_height)
            // var cco = geo_to_car(pt_[0], pt_[1], this.noise_height)
            cco = cco.map(element => { return this.noise_translate + element });
            // console.log("cco", cco)

            // var pnoise = noise.simplex3(cco[0], cco[1], cco[2])
            var pnoise = noise.perlin3(cco[0], cco[1], cco[2])

            pnoise = d3.scaleLinear().domain([-1, 1])
                .range([-this.world_elevation, this.world_elevation])(pnoise)
            pnoise = Math.round(pnoise)

            this.world_elevation_min = Math.min(this.world_elevation_min, pnoise)
            this.world_elevation_max = Math.max(this.world_elevation_max, pnoise)

            point_obj.properties.elevation = pnoise

        })
        // console.log("sites", sites)


    }





    public tighten_points = () => {

        var med_points = []
        this.diagram.polygons().features.forEach(feat_ => {

            var orig_point = feat_.properties.site

            var coords_ = feat_.properties.sitecoordinates
            var centroid_ = d3.geoCentroid(feat_)
            // console.log("feat_:", feat_)
            // console.log("centroid_:", centroid_)
            var do_tighten = true
            if (coords_[0] == 0 && Math.abs(coords_[1]) == 90)
                do_tighten = false

            if (do_tighten)
                orig_point.coordinates = [
                    Math.round((coords_[0] + centroid_[0]) / 2),
                    Math.round((coords_[1] + centroid_[1]) / 2),
                ]

            // console.log("orig_point:", orig_point)
            med_points.push(orig_point)

        });


        this.generate_world(med_points)
    }



    public initialize_map = () => {

        var seed_vpoints = this.generate_some_points()

        // console.log("seed_vpoints", seed_vpoints)
        // seed_vpoints = d3.shuffle(seed_vpoints)

        this.rm_dup_points(seed_vpoints)
        var geo_vpoints = this.make_geo_points(seed_vpoints)

        this.generate_world(geo_vpoints)

    }


    public generate_world = (cells_points_) => {



         this.geopt_add_perlin_height(cells_points_);

        this.make_topo_map(cells_points_)


        // console.log("cells_points_", cells_points_)

        console.log("Altitude min max", [this.world_elevation_min, this.world_elevation_max])


        this.diagram = this.make_voronoi(cells_points_);
        this.calculate_altitude_colors(this.world_elevation_min, this.world_elevation_max)

        this.compute_graph(this.diagram)

        this.update_geo_mesh()
    }

    world_elevation_min = 0
    world_elevation_max = 0




    world_graph: any = null

    public compute_graph = (diagram_) => {
        // console.log("diagram_", diagram_)
        this.world_graph = { sources: [], targets: [], costs: [] };
        var list_data = []


        // diagram_.delaunay.neighbors.forEach((neigs_, index) => {
        //     console.log("neighbors.forEach", neigs_, index)
        // });

        var geo_polys = diagram_.polygons().features
        geo_polys.forEach((elem_, index1_, arr_) => {
            var pt1_ = elem_.properties.site
            // console.log("elem_", index1_, elem_)
            var cost1_ = pt1_.properties.elevation

            elem_.properties.neighbours.forEach(neeee_ => {
                var pt2_ = geo_polys[neeee_]
                var cost2_ = pt2_.properties.site.properties.elevation

                // console.log("neeee_", neeee_,cost2_,pt2_)

                const costs = [
                    this.costs_of_2_elevations(cost1_, cost2_),
                    this.costs_of_2_elevations(cost2_, cost1_)
                ]

                this.world_graph.sources.push(index1_);
                this.world_graph.targets.push(neeee_);
                this.world_graph.costs.push(costs[0]);

                this.world_graph.sources.push(neeee_);
                this.world_graph.targets.push(index1_);
                this.world_graph.costs.push(costs[1]);

                list_data.push([cost1_, cost2_, costs[0]])
                list_data.push([cost2_, cost1_, costs[1]])

            });


        });


        if (this.show_elev_cost_heatmap)
            this.view_data(list_data)

    }

    public calculate_altitude_colors = (min: number, max: number) => {

        var rel_sc = d3.scaleLinear().domain([0, 100]).range([min, max])
        var sc_data = [
            [this.rgba(21, 15, 131, 1), rel_sc(0)],
            [this.rgba(23, 23, 193, 1), rel_sc(22)],
            [this.rgba(20, 154, 200, 1), rel_sc(36)],
            [this.rgba(200, 181, 29, 1), rel_sc(43)],
            [this.rgba(31, 182, 46, 1), rel_sc(52)],
            [this.rgba(40, 159, 29, 1), rel_sc(70)],
            [this.rgba(80, 70, 70, 1), rel_sc(85)],
            [this.rgba(70, 50, 50, 1), rel_sc(90)],
        ]

        // console.log(sc_data)
        this.relief_gradient = d3
            .scaleLinear()
            .domain(sc_data.map(d => { return <number>d[1] }))
            .range(sc_data.map(d => { return <number>d[0] }))
    }

    public make_topo_map = (points_) => {
        var ctr_ = d3g.geoContour()
            .thresholds(d3.range(-300, 300, 25))

        var cont_points = points_.map(function (pts_) {
            return [].concat(pts_.coordinates).concat([pts_.properties.elevation])
        })

        // contour_points_feat = ctr_(cont_points)

        console.log("contour_points_feat", this.contour_points_feat)
    }




    public key_press = () => {
        console.log(d3.event.key, "!!!!!!!!!!!!!!!!!")
        switch (d3.event.key) {
            case "t":
                this.tighten_points();
                break;

            case "q":
                this.show_points = !this.show_points
                this.updated_geo_points();
                break;

            case "W":
                this.show_elev_cost_heatmap = !this.show_elev_cost_heatmap
                this.compute_graph(this.diagram)
                // update_geo_mesh();
                break;

            case "w":
                this.ignore_elevation_cost = !this.ignore_elevation_cost
                this.compute_graph(this.diagram)
                this.update_geo_mesh();
                break;

            case "o":
                this.aprox_points -= 100;
                this.aprox_points = Math.max(0, this.aprox_points)
                this.initialize_map(); break;
            case "p":
                this.aprox_points += 100;
                this.aprox_points = Math.max(0, this.aprox_points)
                this.initialize_map(); break;

            default:
                console.log("Pressed", d3.event)
                break;
        }


    }




}