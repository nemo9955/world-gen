


import * as d3geo from "d3-geo";

export function make_sphere_points_rand_unif(number) {
    const degrees = 180 / Math.PI
    return Array.from({ length: number }, (d) => { return [Math.random() * 360, Math.acos(2 * Math.random() - 1) * degrees - 90]; });
}


// https://observablehq.com/@kemper/voronoi-polygon-smoothing
// https://observablehq.com/@mbostock/poisson-disc-distribution
// https://www.jasondavies.com/poisson-disc/
export function poissonDiscSamplerSphere(cells_count) {
    const radius = Math.sqrt((0.43 * (180 * 2 * 90 * 2)) / cells_count); // TODO 0.43 seems OK on a sphere
    var rng = Math.random
    const width = 360
    const height = 2 * 90
    const points = [],
        max = (3 * (width * height)) / (radius * radius),
        sampleSites = sampler(width, height, radius);

    for (var i = 0; i < max; ++i) {
        let s = sampleSites();
        if (s) points.push(s);
    }
    return points;

    function sampler(width, height, radius) {
        var k = 40, // maximum number of samples before rejection
            radius2 = radius * radius,
            R = 3 * radius2,
            cellSize = radius * Math.SQRT1_2,
            gridWidth = Math.ceil(width / cellSize),
            gridHeight = Math.ceil(height / cellSize),
            grid = new Array(gridWidth * gridHeight),
            queue = [],
            edge_points__ = [],
            queueSize = 0,
            sampleSize = 0;

        // console.log("cellSize", cellSize);
        // console.log("gridWidth", gridWidth);
        // console.log("gridHeight", gridHeight);
        // console.log("radius", radius);
        // console.log("radius2", radius2);

        return function () {
            // if (!sampleSize) return sample(rng() * width, rng() * height);
            if (!sampleSize) return sample(1, 1);

            // Pick a random existing sample and remove it from the queue.
            while (queueSize) {
                var i = (rng() * queueSize) | 0,
                    s = queue[i];

                // Make a new candidate between [radius, 2 * radius] from the existing sample.
                for (var j = 0; j < k; ++j) {
                    var a = 2 * Math.PI * rng(),
                        r = Math.sqrt(rng() * R + radius2),
                        x = s[0] + r * Math.cos(a),
                        y = s[1] + r * Math.sin(a);

                    // Reject candidates that are outside the allowed extent,
                    // or closer than 2 * radius to any existing sample.
                    if (0 <= x && x < width && -(height / 2) <= y && y < (height / 2) && far(x, y))
                        return sample(x, y);
                }

                queue[i] = queue[--queueSize];
                queue.length = queueSize;
            }
        };

        function far(x, y) {
            var i = (x / cellSize) | 0,
                j = ((y + (height / 2)) / cellSize) | 0,
                i0 = Math.max(i - 2, 0),
                j0 = Math.max(j - 2, 0),
                i1 = Math.min(i + 3, gridWidth),
                j1 = Math.min(j + 3, gridHeight);

            for (j = j0; j < j1; ++j) {
                var o = j * gridWidth;
                for (i = i0; i < i1; ++i) {
                    if ((s = grid[o + i])) {
                        var s

                        const somegeodist = d3geo.geoDistance([s[0], s[1]], [x, y]) * 180 / Math.PI
                        if (somegeodist < radius) return false;
                    }
                }
            }


            for (let index = 0; index < edge_points__.length; index++) {
                const element = edge_points__[index];
                if ((s = grid[element])) {
                    var s
                    const somegeodist = d3geo.geoDistance([s[0], s[1]], [x, y]) * 180 / Math.PI
                    if (somegeodist < radius) return false;
                }
            }


            return true;
        }

        function sample(x, y) {
            var s = [x, y];
            queue.push(s);
            const pos_ = gridWidth * (((y + (height / 2)) / cellSize) | 0) + ((x / cellSize) | 0)
            grid[pos_] = s;
            ++sampleSize;
            ++queueSize;

            if (Math.abs(y) > (90 - (cellSize * 2)) || Math.abs(x - (width / 2)) > (180 - (cellSize * 2))) {
                edge_points__.push(pos_)
            }

            return s;
        }
    }
}



