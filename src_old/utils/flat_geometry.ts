


import { default as geom } from "@flatten-js/core"


export class GenericFlatPoint {

    [index: number]: number;

    constructor(x?: number, y?: number) {
        this.set(x, y)
    }


    public point = new geom.Point(0, 0)
    public get x(): number { return this.point.x; }
    public set x(v: number) { this.point.x = v; this[0] = v; }

    public get y(): number { return this.point.y; }
    public set y(v: number) { this.point.y = v; this[1] = v; }

    public set(x_: number, y_: number) { this.x = x_; this.y = y_; }
    public get coordinates(): [number, number] { return [this.x, this.y]; }




}


export class GenericSpherePoint {

    [index: number]: number;

    constructor(x?: number, y?: number) {
        this.set(x, y)
    }

    public get x(): number { return this[0]; }
    public set x(v: number) { this[0] = v; }

    public get y(): number { return this[1]; }
    public set y(v: number) { this[1] = v; }

    public set(x_: number, y_: number) { this.x = x_; this.y = y_; }
    public get coordinates(): [number, number] { return [this.x, this.y]; }




}

