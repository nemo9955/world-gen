

import * as d3 from "d3";
import * as d3g from "d3-geo-voronoi";
import * as d3d from "d3-delaunay";
import * as d3t from "d3-tricontour";
import * as dju from "../../libs/dij_utils.js";
import * as geom_util from "../utils/flat_geometry";

import { WorldCells, WorldCell } from "./WorldCells";







export class TectonicPlate extends geom_util.GenericSpherePoint {
    neighbours = new Array<TectonicPlate>()
    id: number
    poly_coords: any
    geo_poly: any
    geo_point: any
    constructor(x: number, y: number) {
        super(x, y)
    }
}



export class WorldData {

    private _tectonic_pt = new Array<TectonicPlate>()

    private cells_ = new WorldCells()

    constructor() {

    }



    public seed_cells = (seed_cells_: Array<any>) => {
        this.cells_.seed_cells(seed_cells_)
    }



    private make_geo_point = (cell: WorldCell) => {
        return {
            type: "Point",
            coordinates: cell.coordinates,
            properties: {
                id: cell.id,
                size: 5
            }
        }
    }


    public get cells(): Array<WorldCell> {
        return this.cells_.cells;
    }

    public get tectonic_plates(): Array<TectonicPlate> {
        return this._tectonic_pt;
    }

    // public set cells(v: Array<WorldCell>) {
    //     this._cells = v
    //     this.update_cells()
    // }


    public seed_tectocic_plates(tec_plates_: Array<any>) {
        this._tectonic_pt = tec_plates_.map((var_) => {
            var tecp = new TectonicPlate(var_[0], var_[1])
            tecp.geo_point = this.make_geo_point(tecp)
            return tecp
        })
    }



}