

import { WorldData } from "./WorldData";
import { WorldEditor } from "./WorldEditor";
import { WorldCell } from "./WorldCells";


import * as d3 from "d3";
import * as d3g from "d3-geo-voronoi";
import * as d3d from "d3-delaunay";
import * as d3t from "d3-tricontour";
import * as dju from "../../libs/dij_utils.js";
import * as d3geo from "d3-geo";


export class WorldView {

    view_width: number
    view_height: number

    initial_scale: any
    initial_rotate: any

    projection: any
    geoPath: any


    world_map_svg = d3.select('body').append('svg')


    graticule = d3.geoGraticule();
    // graticule = d3geo.geoGraticule()


    // grat = this.world_map_svg.append("g").append('path')
    polygon = this.world_map_svg.append("g").selectAll("*")
    cell_points = this.world_map_svg.append("g").selectAll("*")
    tectonic_points = this.world_map_svg.append("g").selectAll("*")

    grat = this.world_map_svg.append("g").append("path")



    private world_edit_: WorldEditor
    private world_data_: WorldData

    constructor(world_ed_: WorldEditor) {
        this.resize()

        this.world_edit_ = world_ed_
        this.world_data_ = this.world_edit_.world_data_


        this.initial_scale = 300;
        this.initial_rotate = {
            x: 0,
            y: 0
        };



        // this.projection = d3.geoOrthographic(); this.initial_scale = 300
        this.projection = d3.geoNaturalEarth1(); this.initial_scale = 150
        // this.projection = d3.geoMercator(); this.initial_scale = 140



        this.projection.scale(this.initial_scale)
            .translate([this.view_width / 2, this.view_height / 2])
            .rotate([this.initial_rotate.x, this.initial_rotate.y])
            .center([0, 0])

        this.geoPath = d3.geoPath()
            .projection(this.projection);


        this.geoPath.pointRadius(function (d: any) {
            // console.log(d)
            try {
                return d.properties.size;
            }
            catch (error) {
                return 5;
            }
        })


        this.resize()



        this.world_map_svg.on("click", this.clicked)
        d3.select(window).on("resize", this.update_view)
        // d3.select("body").on("keypress", this.key_press)


        this.world_map_svg.call(d3.zoom().on('zoom', this.zoomed));

        // this.world_map_svg.on("mousemove", this.draw_paint_brush)
        //     .on("mousedown", this.mouseDown)
        //     .on("mouseup", this.mouseUp)



        // this.initialize_map()
        this.draw()
    }


    public update_view = () => {
        this.resize()
        this.draw()
    }


    public draw = () => {

        this.polygon = this.polygon.data(this.world_data_.cells, this.get_cell_id)
            .join(
                enter => enter.append("path")
                    .call(this.update_polygon)
                // .style("stroke", "#A00")
                ,
                update => update
                    .call(this.update_polygon),
                exit => exit.remove()
            )



        // this.cell_points = this.cell_points
        //     .data(this.world_data_.cells, this.get_cell_id)
        //     .join(
        //         enter => enter.append("path")
        //             .attr("class", "point")
        //             .attr("fill", "white")
        //             .style("stroke", "red")
        //             .attr('opacity', ".3")
        //             .attr('stroke-width', "1px")
        //             .attr("d", this.get_point_fun),
        //         update => update.attr("d", this.get_point_fun)
        //         ,
        //         exit => exit.remove()
        //     )


        this.tectonic_points = this.tectonic_points
            .data(this.world_data_.tectonic_plates, this.get_cell_id)
            .join(
                enter => enter.append("path")
                    .attr("class", "point")
                    .attr("fill", "cyan")
                    .style("stroke", "red")
                    .attr('stroke-width', "2px")
                    .attr("d", this.get_point_fun),
                update => update.attr("d", this.get_point_fun),
                exit => exit.remove()
            )



        this.grat = this.grat.datum(this.graticule)
            .join(
                enter => enter.append("path"),
                update => update
                    .attr('d', this.geoPath)
                    .attr('fill', "none")
                    .style("stroke", "red")
                    .attr('stroke-width', "1px")
                    .attr('stroke-opacity', ".3")
                ,
                exit => exit.remove()
            )



    }



    public get_cell_id = (params: any) => {
        return params.id
    }



    public update_polygon = (poly_) => {
        poly_
            .style("stroke", "#000")
            .attr("d", (poly_: WorldCell) => {
                // console.log("poly_", poly_);
                // console.log("poly_.poly_coords", poly_.poly_coords);
                // console.log("this.geoPath(poly_.poly_coords)", this.geoPath(poly_.poly_coords));
                // console.log("this.geoPath", this.geoPath);
                return this.geoPath(poly_.geo_poly)
            })
            // .attr("fill", "#f4f4f4")
            .attr("fill", function (d) {
                // console.log("fill poly ... ", d.properties.site.properties.elevation, d)
                // return "#f4f4f4"

                return "green"

                // var loc_col = this_.relief_gradient(d.properties.site.properties.elevation)
                // var polsc = d3
                //     // .scaleSqrt()
                //     .scalePow<string>().exponent(0.2)
                //     .domain([0, 60, 90])
                //     .range([loc_col, loc_col, "white"])
                // // .range(["white", loc_col])
                // var fin_col = polsc(Math.abs(d.properties.sitecoordinates[1]))

                // return fin_col
            })
    }


    public get_point_fun = (point_) => {
        return this.geoPath(point_.geo_point)
    }




    public zoomed = () => {
        var transform = d3.event.transform;


        var window_yaw = d3.scaleLinear()
            .domain([-this.view_width, this.view_width])
            .range([-180, 180])

        var window_pitch = d3.scaleLinear()
            .domain([-this.view_height, this.view_height])
            .range([90, -90]);


        var r = {
            x: window_yaw(transform.x),
            y: window_pitch(transform.y)
        };
        // var k = Math.sqrt(100 / projection.scale());

        if (d3.event.sourceEvent.wheelDelta) {
            this.projection.scale(this.initial_scale * transform.k)
        } else {
            this.projection.rotate([this.initial_rotate.x + r.x, this.initial_rotate.y + r.y]);
        }

        // this.redraw()
    };


    public resize = () => {
        this.view_width = window.innerWidth - 50;
        this.view_height = window.innerHeight - 50;

        this?.world_map_svg
            .attr("width", this.view_width)
            .attr("height", this.view_height)



        this?.projection
            // ?.scale(initial_scale)
            ?.translate([this.view_width / 2, this.view_height / 2])
            ?.center([0, 0])
        // .rotate([initial_rotate.x, initial_rotate.y])




    }

    public clicked = (d, i, n) => {
        console.log("CLICK ...", d3.event)
        // if (d3.event.shiftKey) {
        //     this.findcell(this.projection.invert(d3.mouse(n[i])));
        // }
        // else if (d3.event.ctrlKey) {
        //     var m = this.projection.invert(d3.mouse(n[i]))

        //     var found = this.diagram.find(m[0], m[1], 0);
        //     // var cobj = polygon._groups[0][found]
        //     var cobj = this.diagram.polygons().features[found]
        //     var coid = this.get_poly_id(cobj)
        //     console.log("CLICKED ON : ", found, cobj)

        //     if (coid in this.general_points_list) {
        //         delete this.general_points_list[coid]
        //     } else {
        //         this.general_points_list[coid] = JSON.parse(JSON.stringify(cobj.properties.site))
        //         this.general_points_list[coid].properties.size = 8
        //         console.log("general_points_list[coid] : ", this.general_points_list[coid])
        //     }

        //     // console.log("POINTS ...", general_points_list)
        //     // updated_geo_points()
        //     this.update_geo_mesh()

        // }
        // else {

        //     // console.log("polygon", polygon);

        // }
    }



}