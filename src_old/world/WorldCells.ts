
import * as geom_util from "../utils/flat_geometry";

import * as d3 from "d3";
import * as d3g from "d3-geo-voronoi";
import * as d3d from "d3-delaunay";
import * as d3t from "d3-tricontour";
import * as dju from "../../libs/dij_utils.js";

export class WorldCell extends geom_util.GenericSpherePoint {
    neighbours = new Array<WorldCell>()
    id: number
    poly_coords: any
    geo_poly: any
    geo_point: any
    constructor(x: number, y: number) {
        super(x, y)
    }
}


export class WorldCells {


    private _cells = new Array<WorldCell>()

    constructor() {
    }



    public seed_cells = (seed_cells_: Array<any>) => {
        var voronoi = d3g.geoVoronoi()(seed_cells_);

        // console.log("voronoi", voronoi);
        // console.log("voronoi.polygons()", voronoi.polygons());

        this._cells = voronoi.polygons().features.map((cel_, ind_) => {
            // console.log("cel_, ind_", cel_, ind_);
            const coords = cel_.properties.sitecoordinates
            var cell = new WorldCell(coords[0], coords[1])
            cell.geo_poly = cel_.geometry
            cell.geo_point = this.make_geo_point(cell)
            cell.id = ind_

            return cell
        });

        voronoi.polygons().features.forEach((cel_, ind_) => {
            var cell = this._cells[ind_]
            cell.neighbours = []
            cel_.properties.neighbours.forEach(nid_ => {
                cell.neighbours.push(this.cells[nid_])
            });

            return cell
        });

        console.log("this.cells", this.cells);
    }




    private make_geo_point = (cell: WorldCell) => {
        return {
            type: "Point",
            coordinates: cell.coordinates,
            properties: {
                id: cell.id,
                size: 5
            }
        }
    }


    public get cells(): Array<WorldCell> {
        return this._cells;
    }



}