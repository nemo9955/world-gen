


import { WorldData } from "./WorldData";

import * as points_utils from "../utils/sphere_geometry";

import * as d3 from "d3";
import * as d3g from "d3-geo-voronoi";
import * as d3geo from "d3-geo";
import * as d3d from "d3-delaunay";
import * as d3t from "d3-tricontour";
import * as dju from "../../libs/dij_utils.js";

export class WorldEditor {

    cells_count: any = 100

    constructor(public world_data_: WorldData) {

        this.initialize_map()
        this.make_tectonic_plates()

    }


    public initialize_map = () => {

        var seed_vpoints = this.generate_some_points()

        // console.log("seed_vpoints", seed_vpoints)
        // seed_vpoints = d3.shuffle(seed_vpoints)

        // var geo_vpoints = this.make_geo_points(seed_vpoints)

        // console.log("seed_vpoints", seed_vpoints);

        this.world_data_.seed_cells(seed_vpoints)


        // var voronoi = d3g.geoVoronoi()(seed_vpoints);

        // console.log("voronoi", voronoi);
        // console.log("voronoi.polygons()", voronoi.polygons());

        // this.generate_world(geo_vpoints)

        // this.voronoi = voronoi

    }

    public make_tectonic_plates = () => {


        // var tect_pl = points_utils.make_sphere_points_rand_unif(8)
        var tect_pl = points_utils.poissonDiscSamplerSphere(8)
        this.world_data_.seed_tectocic_plates(tect_pl)

    }


    public generate_some_points = () => {

        // var generators = d3.sum([this.generate_fib, this.generate_rand])
        // // console.log("generators", generators)
        // if (generators <= 0) return null

        console.log("Will gen aprox total ", this.cells_count)
        var sites = []

        // sites = sites.concat([[0, -90], [0, 90], [0, 0]])
        // sites = sites.concat([[360, -91]])
        // sites = sites.concat([[360, 91]])
        // sites = sites.concat([[360, 90]])
        // sites = sites.concat([[360, 0]])
        // sites = sites.concat([[360, 180]])
        // sites = sites.concat([[360, -180]])

        // if (this.generate_fib)
        //     sites = sites.concat(this.make_sphere_points_fib_phi(pts_per_gen))

        // if (this.generate_rand)
        // sites = sites.concat(this.make_sphere_points_rand_unif(pts_per_gen))



        sites = sites.concat(points_utils.poissonDiscSamplerSphere(this.cells_count))

        // sites = sites.concat(this.poissonDiscSampler(180 * 2, 90 * 2, 9))

        sites = this.rm_dup_points(sites)

        // sites.forEach(element => {
        //     console.log("element", element);
        // });

        // sites = sites.concat(make_sphere_points_rand_wrong(pts_per_gen))

        console.log("Total generated ", sites.length)

        // console.log("sites", sites.length, sites)
        // console.log("sites", sites.length ,sites.toLocaleString())

        return sites
    }





    public rm_dup_points = (points_) => {


        // points_ = points_.map(pt => [(pt[0] % 360), (pt[1]+90) % 180])

        points_.sort((a, b) => a[1] - b[1])
        points_.sort((a, b) => a[0] - b[0])

        // console.log("points_", points_.length ,points_.toLocaleString())
        // console.log("points_", points_.length, points_)

        for (let index = 0; index < points_.length; index++) {
            const pt_ = points_[index]

            // points_.forEach(function (pt_, i, points_) {
            if (index + 1 >= points_.length) continue;
            var pt2_ = points_[index + 1]

            if (pt_[0] === pt2_[0] && pt_[1] === pt2_[1]) {
                console.log("Removing ", pt2_, " == ", pt_, " >>>", points_[index + 1])
                var len1 = points_.length
                var rmmm = points_.splice(index + 1, 1)
                var len2 = points_.length
                index--
                // console.log("dup", len1, len2, pt_, pt2_, "rmmm",rmmm.toLocaleString())
            }
        }
        // console.log("sites", sites.length ,sites.toLocaleString())
        return points_
    }






}