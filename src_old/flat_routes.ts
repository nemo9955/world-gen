


import { FlatTerrain, Cell } from "./flat_terrain"

import * as geom_util from "./utils/flat_geometry";
import * as misc from "./utils/miscs";
import * as noise_lib from 'noisejs';


class Route {

    constructor(public joint1: Joint, public joint2: Joint) {
    }

}



class Joint extends geom_util.GenericFlatPoint {

    routes = new Array<Route>()
    constructor(public id: number, x: number, y: number) {
        super(x, y)
    }
}



export class FlatRoutes {

    routes = new Array<Route>()
    joints = new Array<Joint>()

    constructor() {

    }

    public addJoint = (x_: number, y_: number) => {
        this.joints.push(new Joint(this.joints.length, x_, y_))
        console.log("x_,y_", x_,y_);
        console.log("this.joints", this.joints);
    }

    public addDrawSvg = (base_svg: any) => {
        return new FlatRoutesDraw(this, base_svg)
    }

}


import * as d3 from "d3";

export class FlatRoutesDraw {



    d3_line = d3.line();


    routes_draw: any = this.base_svg.append("g").selectAll("*")
    joints_draw: any = this.base_svg.append("g").selectAll("*")

    constructor(public routes: FlatRoutes, public base_svg: any) {

    }

    public draw = () => {





        this.joints_draw = this.joints_draw.data(this.routes.joints, this.get_joint_id)
            .join(
                enter => enter
                    .append("circle")
                    .style("opacity", 0.3)
                    .style("r", 5)
                    .attr("fill", "yellow")
                    .attr("cx", function (d) { return d[0]; })
                    .attr("cy", function (d) { return d[1]; })
                ,
                update => update
                    .attr("d", this.d3_line)
                ,
                exit => exit.remove()
            )

    }


    private get_joint_id = (jo_: Joint) => {
        return jo_.id
    }

}