
// import { MapWorld } from "./MapWorld";


import { WorldEditor } from "./world/WorldEditor";
import { WorldView } from "./world/WorldView";
import { WorldData } from "./world/WorldData";


import {World, System} from "ecsy";



export function start_world() {

    // var map_  = new MapWorld()

    var world_data_ = new WorldData();
    var world_edit_ = new WorldEditor(world_data_);
    var world_view_ = new WorldView(world_edit_);


}
