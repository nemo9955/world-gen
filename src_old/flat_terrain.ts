


import * as d3 from "d3";
import * as d3d from "d3-delaunay";
import * as d3t from "d3-tricontour";

import * as dju from "../libs/dij_utils.js";
import * as misc from "./utils/miscs";
import * as geom_util from "./utils/flat_geometry";

import { default as geom } from "@flatten-js/core"
import * as noise_lib from 'noisejs';



export class Cell extends geom_util.GenericFlatPoint {

    elevation: number = 0
    river_val: number = 0
    id: number = 0
    is_river: boolean = false
    type: string = null

    poly: any = null

    constructor(x?: number, y?: number) {
        super(x, y)
    }

}


export class FlatTerrain {


    noise_spread: any = 30
    noise_seed: any = Math.random()
    noise_translate: any = (100 + Math.round(Math.random() * 999))

    voronoi: d3d.Voronoi<Cell>
    delaunay: d3d.Delaunay<Cell>

    points_obj = new Array<Cell>()


    elevation_val_minmax: any = [null, null]
    river_val_minmax: any = [null, null]
    world_graph: any


    constructor() {

    }



    private _points_cnt: number = 500
    public get points_cnt(): number { return this._points_cnt; }
    public set points_cnt(v: number) { this._points_cnt = v; }

    private _points_arrange: string = "poisson"
    public get points_arrange(): string { return this._points_arrange; }
    public set points_arrange(v: string) { this._points_arrange = v; }


    public _terrain_height: number = 500
    public get terrain_height(): number { return this._terrain_height; }
    public set terrain_height(v: number) { this._terrain_height = v; }

    public _terrain_width: number = 500
    public get terrain_width(): number { return this._terrain_width; }
    public set terrain_width(v: number) { this._terrain_width = v; }

    public set_size = (w_: number, h_: number) => {
        this.terrain_width = w_
        this.terrain_height = h_
    }


    public generate_terrain = (draw_data_: any) => {
        this.generate_points()

        this.points_mk_elevation()

        this.calculate_rivers()

        this.world_graph = this.compute_graph(this.points_obj, this.get_graph_cell_elevation)
        // if (this.show_elev_cost_heatmap)
        //     this.view_data(this.world_graph.listed)

        this.find_best_spots(draw_data_)

        // this.misc_debuggs()
    }

    private generate_points = () => {
        var rpt_coords_ = dju.pick2d(this.terrain_width, this.terrain_height, this.points_cnt, this.points_arrange);
        misc.rm_dup_points(rpt_coords_)
        console.log("Total generated ", rpt_coords_.length)
        this.points_obj = this.calc_cell_points(rpt_coords_)

        // this.check_points_difference(this.points_obj)
    }





    public find_best_spots = (draw_data_: any) => {


        var center_point = geom.point(this.terrain_width / 2, this.terrain_height / 2)
        var trvec = geom.vector(0.0, 10000)

        var out_pts_ = []
        out_pts_ = d3.range(0, 361, 40).map((deg) => {
            var rad = deg * (Math.PI / 180)
            // console.log("deg , rad", deg , rad);
            return center_point.clone().translate(trvec).rotate(rad, center_point)
        })
        // out_pts_.push(center_point)


        console.log("this.delaunay", this.delaunay);

        // console.log("out_pts_", out_pts_);
        // console.log("this.points_obj", this.points_obj);

        var ext_or_id = []


        out_pts_.forEach(pt_ => {
            // this.draw_some_points.push([pt_.x, pt_.y])

            var pt_in = this.delaunay.find(pt_.x, pt_.y);
            // console.log("pid", pt_in);
            // console.log("this.points_obj[pid]", this.points_obj[pt_in]);

            var ptob_ = this.points_obj[pt_in].coordinates

            ext_or_id.push(pt_in)
            // this.draw_some_points.push([ptob_[0], ptob_[1]])
        });


        console.log("ext_or_id", ext_or_id);

        var real_elev = this.compute_graph(this.points_obj, this.get_graph_cell_elevation)
        // var real_elev=  this.world_graph

        var tree = dju.shortest_tree({
            graph: real_elev,
            origins: ext_or_id,
            // cutoff: 40000,
        })
        var tree_fin = [...tree][0]

        console.log("tree_fin", tree_fin);

        // var juncts = dju.shortest_junctions(real_elev, tree_fin)
        // console.log("juncts", juncts);


        var cont_points = this.points_obj.map(function (pts_) {
            return [].concat(pts_.coordinates)
        })


        for (let i = 0; i < ext_or_id.length; i++) {

            var pt_cont = d3t.tricontour()
                .value((d, j) => (tree_fin.origin[j] === ext_or_id[i] ? 1 : 0))
                .contour(cont_points, 0.6)

            // contour_points_feat = ctr_(cont_points, 0.6)
            // geo_dij_contours = ctr_(cont_points, 0.6)
            // var pt_cont = ctr_(cont_points, 0.6)
            draw_data_.geo_dij_contours.push(pt_cont)

            // console.log("cont_points", cont_points)
            // console.log("contour_points_feat", contour_points_feat)
            // console.log("pt_cont", pt_cont)
        }
        console.log("this.geo_dij_contours", draw_data_.geo_dij_contours)




        var paths_vals = []


        var path = dju.shortest_paths(real_elev, tree_fin)
        console.log("!!!!!!!!!! path", path);

        path.forEach(pth_ => {
            var pth_lines = []
            pth_.path.forEach((id_, inx_, arr) => {
                paths_vals.push(id_)

                var [pa, pb] = this.points_obj[id_].coordinates
                pa += (Math.random() * 20) - 10
                pb += (Math.random() * 20) - 10

                pth_lines.push([pa, pb])
            });
            // this.draw_some_lines.push(pth_lines)
            // this.river_paths.push(pth_lines)
        });



    }









    public points_mk_elevation = () => {
        var noise = new noise_lib.Noise(this.noise_seed);

        console.log("noise_seed", this.noise_seed, "noise_translate", this.noise_translate)


        var noise_to_elev = d3.scaleLinear()
            .domain([-1, 1])
            .range([5, 10])
        // .range([-this.elevation_steps, this.elevation_steps])


        this.points_obj.forEach((pt_obj_, index) => {
            var pt_ = pt_obj_.coordinates.map(element => { return ((this.noise_spread * (element + 1000)) / 10000) });

            if (pt_[0] <= 0 || pt_[1] <= 0)
                console.log("points_mk_elevation pt_obj_.coordinates", pt_obj_.coordinates, pt_);


            var pnoise = noise.perlin3(pt_[0], pt_[1], this.noise_translate)
            // var pnoise = noise.perlin2(pt_[0], pt_[1])
            // var pnoise = noise.simplex3(pt_[0], pt_[1], this.noise_translate)
            // var pnoise = noise.simplex2(pt_[0], pt_[1])

            // this.noise_translate

            var eleval = noise_to_elev(pnoise)
            // eleval = Math.pow(eleval, 2)

            // pt_obj_.elevation = Math.round(eleval)
            pt_obj_.elevation = eleval
        });

        this.update_all_minmax()
    }






    // private ptObjX = (pt_obj_) => { return pt_obj_.x }
    // private ptObjY = (pt_obj_) => { return pt_obj_.y }



    public calc_cell_points = (points_) => {
        // this.voronoi = d3d.Delaunay.from(points_, this.ptObjX, this.ptObjY).voronoi([0, 0, this.terrain_width, this.terrain_height])
        this.voronoi = d3d.Delaunay.from(points_).voronoi([0, 0, this.terrain_width, this.terrain_height])
        this.delaunay = this.voronoi.delaunay

        var re_ord_points = []

        for (let index = 0; index < points_.length; index += 1) {
            var ce_x = this.delaunay.points[(index * 2) + 0]
            var ce_y = this.delaunay.points[(index * 2) + 1]

            var cell = new Cell(ce_x, ce_y)

            cell.type = "Point"
            cell.id = index
            cell.poly = this.voronoi.cellPolygon(index)
            // // console.log("cell", cell);

            re_ord_points.push(cell)
        }

        // this.check_points_difference(re_ord_points)

        return re_ord_points

    }


    // public coord_to_obj_points = (rpt_coords_) => {
    //     return rpt_coords_.map((pt_, index) => {
    //         var pp_: any = {}
    //         pp_.type = "Point"
    //         pp_.coordinates = pt_
    //         pp_.properties = {}
    //         pp_.properties.id = index
    //         pp_.properties.poly = this.voronoi.cellPolygon(index)
    //         return pp_
    //     })
    // }




    public costs_of_2_elevations = (elev1, elev2) => {

        var medelev = ((elev1 + elev2) / 2)
        var diff = Math.abs(elev1 - elev2)
        var dire = Math.sign(elev1 - elev2)


        var diff_fact = ((diff * 2) - (diff * dire * 0.2))
        diff_fact += Math.pow(diff_fact, 2)


        var elev_fact = ((Math.abs(medelev) - (medelev * 0.1)) * 1.7)
        elev_fact += Math.pow(elev_fact, 2)

        var cost = diff_fact + elev_fact
        cost = Math.sqrt(cost)

        return cost

    }




    public get_graph_cell_elevation = (p1_, p2_) => {

        var e1_ = p1_.elevation
        var e2_ = p2_.elevation

        const cost_dis_ = misc.distance(p1_.coordinates, p2_.coordinates)

        var cost_e12_ = this.costs_of_2_elevations(e1_, e2_)
        var cost_e21_ = this.costs_of_2_elevations(e2_, e1_)

        var cost_1_ = cost_e12_
        var cost_2_ = cost_e21_

        cost_1_ *= cost_dis_
        cost_2_ *= cost_dis_

        return [cost_1_, cost_2_]
    }


    public compute_graph = (points_, cost_calc) => {

        console.log("this.delaunay", this.delaunay);
        console.log("this.voronoi", this.voronoi);

        var graph = { sources: [], targets: [], costs: [], listed: [] };

        points_.forEach((p1_, n1_) => {
            var p1_neigs = this.true_neighbors(n1_)

            // console.log("p1_", p1_);

            p1_neigs.forEach((n2_) => {
                var p2_ = points_[n2_]

                var [cost_1_, cost_2_] = cost_calc(p1_, p2_)
                if (cost_1_ != null && cost_2_ != null) {

                    this.add_double_edge(graph, n1_, n2_, cost_1_, cost_2_)
                }
            });

        });

        var gr_min = Math.min(...graph.costs)
        if (gr_min < 0) {
            var gr_max = Math.max(...graph.costs)
            console.log("Math.min(...graph.costs)", gr_min);
            console.log("Math.max(...graph.costs)", gr_max);
            graph.costs = graph.costs.map(v => v - gr_min)
            graph.listed = graph.listed.map(v => { v[2] -= gr_min; return v })
            console.log("NEW min graph.costs)", Math.min(...graph.costs));
            console.log("NEW max graph.costs)", Math.max(...graph.costs));
        }


        return graph

    }

    public add_double_edge = (gr, e1, e2, c1, c2) => {
        gr.sources.push(e1);
        gr.targets.push(e2);
        gr.costs.push(c1);
        gr.sources.push(e2);
        gr.targets.push(e1);
        gr.costs.push(c2);
        gr.listed.push([e1, e2, c1])
        gr.listed.push([e2, e1, c2])
    }













    river_paths: any



    public calculate_rivers = () => {

        this.points_mk_river_val()

        // this.clicked_points

        var rivgr = this.compute_graph(this.points_obj, this.get_graph_river_noise)

        // this.show_elev_cost_heatmap = true
        // if (this.show_elev_cost_heatmap) {
        //     // this.view_data(rivgr.listed)
        //     this.view_data(null, this.costs_of_2_riv)
        // }

        var river_ends = []
        var river_origs = []

        // river_ends.push([10 + Math.random() * this.terrain_width / 10, 0 + 150])
        // river_ends.push([10 + Math.random() * this.terrain_width / 10, this.terrain_height - 150])

        // river_ends.push([Math.random() * this.terrain_width / 2, 0 - 1000])
        // river_ends.push([Math.random() * this.terrain_width / 2, 0 - 1000])
        river_ends.push([(Math.random() * this.terrain_width / 2) - (this.terrain_width / 3), 0 - 1000])
        river_ends.push([(Math.random() * this.terrain_width / 2) - (this.terrain_width / 3), this.terrain_height + 1000])

        river_ends.forEach(pt_ => {
            var id_ = this.delaunay.find(pt_[0], pt_[1])
            river_origs.push(id_)
            console.log("id_", id_, pt_);
        });
        river_origs = [...new Set(river_origs)];

        // this.draw_some_points = this.draw_some_points.concat(river_ends)



        this.river_paths = []
        var river_cells = []

        // var vals_pts = this.clicked_points.map((id_) => this.points_obj[id_])

        var tree = dju.shortest_tree({
            graph: rivgr,
            origins: river_origs,
            // cutoff: 1000,
        })
        var tree_fin = [...tree][0]


        var path = dju.shortest_paths(rivgr, tree_fin)
        console.log("!!!!!!!!!! path", path);

        path.forEach(pth_ => {
            var pth_lines = []
            pth_.path.forEach((id_, inx_, arr) => {
                river_cells.push(id_)

                var [pa, pb] = this.points_obj[id_].coordinates
                pa += (Math.random() * 20) - 10
                pb += (Math.random() * 20) - 10

                pth_lines.push([pa, pb])
            });
            this.river_paths.push(pth_lines)
        });

        console.log("this.river_paths", this.river_paths);


        this.deepen_from_points(river_cells)

    }



    private update_all_minmax = () => {

        this.elevation_val_minmax = [null, null]
        this.river_val_minmax = [null, null]

        this.points_obj.forEach((pt_obj_, index) => {

            if (pt_obj_?.elevation !== undefined)
                this.elevation_val_minmax = this.update_minmax(this.elevation_val_minmax, pt_obj_.elevation)

            if (pt_obj_?.river_val !== undefined)
                this.river_val_minmax = this.update_minmax(this.river_val_minmax, pt_obj_.river_val)
        })

        console.log("this.elevation_val_minmax", this.elevation_val_minmax);
        // this.relief_gradient = this.get_minmax_color_gradient(this.elevation_val_minmax[0], this.elevation_val_minmax[1])
    }


    private update_minmax = (vec_, val_) => {
        return [Math.min(vec_[0], val_), Math.max(vec_[1], val_)]
    }





    public points_mk_river_val = () => {
        const river_noise_seed = this.noise_seed * 2
        const river_noise_trans = this.noise_translate * 2
        const river_noise_spread = 40

        const riv_mul_min = -50
        const riv_mul_max = +50


        var noise = new noise_lib.Noise(river_noise_seed);
        console.log("river_noise_seed", river_noise_seed, "river_noise_trans", river_noise_trans)

        this.points_obj.forEach((pt_obj_, index) => {
            var pt_ = pt_obj_.coordinates.map(element => { return ((river_noise_spread * (element + 1000)) / 10000) });

            if (pt_[0] <= 0 || pt_[1] <= 0)
                console.log("points_mk_river_val pt_obj_.coordinates", pt_obj_.coordinates, pt_);


            // var pnoise = noise.perlin3(pt_[0], pt_[1], river_noise_trans)
            var pnoise = noise.simplex3(pt_[0], pt_[1], river_noise_trans)
            // pnoise = ((pnoise + 0) / 2) * 3
            pnoise = pnoise * 4
            // pnoise = Math.round(pnoise)

            // pnoise = d3.scaleLinear().domain([-1, 1] )
            //     .range([riv_mul_min, riv_mul_max])(pnoise)

            // pnoise = Math.round(pnoise)

            pt_obj_.river_val = pnoise

            // pt_obj_.properties.river_val = pt_obj_.elevation
            // pt_obj_.properties.river_val -= Math.abs(pt_obj_.elevation * pnoise)



        });

        this.update_all_minmax()

    }



    public get_graph_river_noise = (p1_: Cell, p2_: Cell) => {

        // return this.get_graph_cell_elevation(p1_, p2_) ////////////////////////////////////////////////////////////////////

        var e1_ = p1_.river_val
        var e2_ = p2_.river_val

        const cost_dis_ = misc.distance(p1_.coordinates, p2_.coordinates)
        // // console.log("p1_.coordinates, p2_.coordinates", p1_.coordinates, p2_.coordinates);
        // console.log("cost_dis_", cost_dis_);

        var cost_e12_ = this.costs_of_2_riv(e1_, e2_)
        var cost_e21_ = this.costs_of_2_riv(e2_, e1_)

        var cost_1_ = cost_e12_ + Math.ceil(cost_dis_ / 200)
        var cost_2_ = cost_e21_ + Math.ceil(cost_dis_ / 200)

        // if (p2_?.properties?.is_edge === true && p1_?.properties?.is_edge === true) {
        //     cost_1_ += 1000
        //     cost_2_ += 1000
        // }

        return [cost_1_, cost_2_]

    }



    public costs_of_2_riv = (elev1, elev2) => {

        var medelev = ((elev1 + elev2) / 2)
        var diff = Math.abs(elev1 - elev2)
        var dire = Math.sign(elev1 - elev2)

        var diff_fact = diff * dire * -1
        // diff_fact = Math.pow(diff_fact, 2)
        // diff_fact = 0

        var elev_fact = Math.abs(medelev) + (medelev)
        // elev_fact = Math.pow(elev_fact, 2)
        elev_fact = 0

        // var cost = diff_fact * dire * -1
        var cost = elev_fact + diff_fact
        // cost = Math.sqrt(cost) * dire * -1
        // var cost = diff * dire *-1
        // var cost = elev2 - elev1
        // cost -= diff
        // cost*=2


        // return elev2 - elev1
        // return Math.sign(elev2 - elev1)
        return (Math.sign(elev2 - elev1) + 1) * (elev2 - elev1)

        // if (this.ignore_elevation_cost)
        return cost

    }



    public deepen_from_points = (from_dells_) => {
        var cell_depth = {}

        var depth_ = 1

        from_dells_.forEach((n1_, i_) => {
            cell_depth[n1_] = 1
        })

        var cell_seen = {}
        for (let index = 0; index < depth_; index++) {

            cell_seen = {}
            from_dells_ = [...new Set(from_dells_)];

            from_dells_.forEach((n1_, i_) => {
                if (!cell_seen[n1_]) {
                    from_dells_.push(n1_)
                    cell_seen[n1_] = true
                    if (cell_depth[n1_] == null)
                        cell_depth[n1_] = 1
                    else
                        cell_depth[n1_]++
                }

                // var p1_ = this.points_obj[n1_]
                // var p1_neigs = [...this.delaunay.neighbors(n1_)]
                var p1_neigs = this.true_neighbors(n1_)

                p1_neigs.forEach((n2_) => {
                    // var p2_ = this.points_obj[n2_]
                    if (!cell_seen[n2_]) {
                        cell_seen[n2_] = true
                        from_dells_.push(n2_)
                        if (cell_depth[n2_] == null)
                            cell_depth[n2_] = 1
                        else
                            cell_depth[n2_]++
                    }
                });


            });
        }

        console.log("cell_depth", cell_depth);


        from_dells_.forEach((n1_, i_) => {
            var p1_ = this.points_obj[n1_]

            p1_.is_river = true

            var old_ele = p1_.elevation
            // p1_.elevation = Math.abs((p1_.elevation / 10) * (cell_depth[n1_])) * -1
            // p1_.elevation = (Math.abs(1 * (cell_depth[n1_])) * -1)  - Math.abs(old_ele)
            p1_.elevation = (Math.abs(1 * (cell_depth[n1_])) * -1) - 5

            // console.log(" >>>> ", cell_depth[n1_], old_ele, p1_.elevation);


        })

        this.update_all_minmax()


        // var delaunay = d3d.Delaunay.from(null); // https://bl.ocks.org/Fil/3faaaf1b5f34b03a7a2235bf22e20b73
        // delaunay.findAll = function (x, y, radius) {
        //     const points = delaunay.points,
        //         results = [],
        //         seen = [],
        //         queue = [delaunay.find(x, y)];

        //     while (queue.length) {
        //         const q = queue.pop();
        //         if (seen[q]) continue;
        //         seen[q] = true;
        //         if (Math.hypot(x - points[2 * q], y - points[2 * q + 1]) < radius) {
        //             results.push(q);
        //             for (const p of delaunay.neighbors(q)) queue.push(p);
        //         }
        //     }

        //     return results;
        // }



    }




    public true_neighbors(i) {
        var n = [];
        const ai = new Set((this.voronoi.cellPolygon(i) || []).map(String));
        for (const j of this.delaunay.neighbors(i))
            for (const c of this.voronoi.cellPolygon(j) || [])
                if (ai.has(String(c))) n.push(j);
        n = [...new Set(n)];
        return n;
    }







    public check_points_difference = () => {
        var difcnt = 0

        for (let index = 0; index < this.points_obj.length; index += 1) {
            var pobjc_ = this.points_obj[index].coordinates
            var pdevc_ = [this.delaunay.points[(index * 2) + 0], this.delaunay.points[(index * 2) + 1]]

            if (pobjc_[0] != pdevc_[0] && pobjc_[1] != pdevc_[1]) {
                difcnt++
                console.log("DIFF");
                console.log("pobjc_", pobjc_);
                console.log("pdevc_", pdevc_);

                // this.draw_some_text.push([pobjc_[0], pobjc_[1] + 9, index])
                // this.draw_some_text.push([pdevc_[0], pdevc_[1] - 9, index])

            }

            // var pa_w = this.delaunay.points[index + 0]
            // var pb_w = this.delaunay.points[index + 1]
            // re_ord_points.push([pa_w, pb_w])
        }

        if (difcnt != 0) {
            console.log("difcnt", difcnt);
            console.log("this.delaunay.points.length", this.delaunay.points.length);
            console.log("this.points_obj.length", this.points_obj.length);
        }
    }






}