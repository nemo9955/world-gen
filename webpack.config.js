module.exports = {
    entry: './src/app.ts',
    output: {
        filename: 'app.js',
        library: 'world_gen'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
};
